#!/usr/bin/env python3

"""Definition of class Unit."""

import typing as tp

if tp.TYPE_CHECKING:
    from .ptnet import PTnet


class Unit:
    """A Unit is a set of places that are covered by a binary invariant."""

    def __init__(self, num: int, net: 'PTnet', places: tp.List[str]):
        self.num: int = num
        self.net: 'PTnet' = net
        self.places: tp.List[str] = places
