#!/usr/bin/env python3

"""Provide get_argparser that returns pompet's argument parser."""

import argparse

from .commands import compile_net, compile_checker, to_prod, to_pnml


def get_argparser() -> argparse.ArgumentParser:
    """Return pompet's argument parser."""
    def add_shuffle_option(parser: argparse.ArgumentParser) -> None:
        parser.add_argument(
            '-s', '--shuffle', default=False, action='store_true',
            help='shuffle the net description'
        )
    def add_reduce_option(parser: argparse.ArgumentParser) -> None:
        parser.add_argument(
            '-r', '--reduce', default=False, action='store_true',
            help='reduce the net by agglomerating transitions'
        )
    def add_verbose_option(parser: argparse.ArgumentParser) -> None:
        parser.add_argument(
            '-v', '--verbose', action='store_true',
            help='be verbose'
        )
    def add_pnml_argument(parser: argparse.ArgumentParser) -> None:
        parser.add_argument(
            'pnml', type=str,
            help='input pnml file'
        )

    result = argparse.ArgumentParser()

    subparsers = result.add_subparsers()

    # action compile-net
    def command_compile_net(args: argparse.Namespace) -> None:
        compile_net(
            args.pnml, args.exe, shuffle=args.shuffle,
            reduce=args.reduce, dep_rel_file=args.d
        )
    parser = subparsers.add_parser(
        'compile-net',
        help='compile a pnml net into an executable'
    )
    add_pnml_argument(parser)
    parser.add_argument(
        'exe', type=str,
        help='output executable file'
    )
    add_shuffle_option(parser)
    add_reduce_option(parser)
    add_verbose_option(parser)
    parser.add_argument(
        '-d', type=str,
        help='dynamic dependency relation file'
    )
    parser.set_defaults(command=command_compile_net)

    # action compile-checker
    def command_compile_checker(args: argparse.Namespace) -> None:
        compile_checker()
    parser = subparsers.add_parser(
        'compile-checker',
        help='compile the model checking library'
    )
    add_verbose_option(parser)
    parser.set_defaults(command=command_compile_checker)

    # action to-prod
    def command_to_prod(args: argparse.Namespace) -> None:
        to_prod(args.pnml, args.prod, shuffle=args.shuffle, reduce=args.reduce)
    parser = subparsers.add_parser(
        'to-prod',
        help='convert the pnml file to prod input language'
    )
    add_pnml_argument(parser)
    parser.add_argument(
        'prod', type=str,
        help='output prod file'
    )
    add_shuffle_option(parser)
    add_reduce_option(parser)
    add_verbose_option(parser)
    parser.set_defaults(command=command_to_prod)

    # action to-pnml
    def command_to_pnml(args: argparse.Namespace) -> None:
        to_pnml(args.pnml, args.out, shuffle=args.shuffle, reduce=args.reduce)
    parser = subparsers.add_parser(
        'to-pnml',
        help='convert the pnml file to pnml'
    )
    add_pnml_argument(parser)
    parser.add_argument(
        'out', type=str,
        help='output pnml file'
    )
    add_shuffle_option(parser)
    add_reduce_option(parser)
    add_verbose_option(parser)
    parser.set_defaults(command=command_to_pnml)
    return result
