#!/usr/bin/env python3

"""Define some paths to store pompet's data."""

import os
import pathlib


home_dir = os.path.join(pathlib.Path.home(), '.local', 'share', 'pompet')
lib_dir = os.path.join(home_dir, 'lib')
checker_dir = os.path.join(home_dir, 'checker')

checker_lib_file = os.path.join(lib_dir, 'libcheckerpompet.a')
config_file = os.path.join(home_dir, 'config.json')


def init() -> None:
    """Initialise the directory structure of pompet."""
    for path in [home_dir, checker_dir, lib_dir]:
        pathlib.Path(path).mkdir(parents=True, exist_ok=True)
