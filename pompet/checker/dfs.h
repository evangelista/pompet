#ifndef LIB_DFS
#define LIB_DFS

#include "includes.h"
#include "hset.h"
#include "ptnet.h"
#include "config.h"
#include "ptnet_loader.h"
#include "edge_lean.h"
#include "attrs.h"

typedef enum {
  DFS_POP_OK,
  DFS_POP_REVISIT,
  DFS_POP_EMPTY
} dfs_pop_result_t;

typedef struct {
  hset_ref_t ref;
  mem_size_t heap_pos;
  uint16_t no_en;
  uint16_t cur;
  trid_t * en;
} dfs_stack_item_t;

typedef bool_t (* prov_pop_marking) (dfs_stack_item_t *);
typedef void (* prov_pop_marking_post) (dfs_stack_item_t *);
typedef bool_t (* prov_back_edge) (dfs_stack_item_t *);

typedef struct {
  marking_t now;
  hset_mdata_t mdata;
  heap_t heap;
  darray_t stack;
  heap_t stack_heap;
  stub_data_t stub_data;
  uint32_t fexpanded;
  prov_pop_marking prov_pop;
  prov_pop_marking_post prov_pop_post;
  prov_back_edge prov_back;
} dfs_data_t;

extern dfs_data_t DFS;


void
dfs
();

#endif
