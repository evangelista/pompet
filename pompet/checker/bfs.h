#ifndef LIB_BFS
#define LIB_BFS

#define BFS_QUEUE_SLOT_SIZE 100000

#include "includes.h"
#include "hset.h"
#include "list.h"

typedef struct {
  hset_ref_t ref;
  bool_t fexpand;
} bfs_queue_item_t;

typedef struct {
  uint32_t next;
  uint32_t no_items;
  bfs_queue_item_t items[BFS_QUEUE_SLOT_SIZE];
} bfs_queue_slot_t;

typedef list_t bfs_queue_t;

typedef struct {
  bfs_queue_t queue;
} bfs_data_t;

extern bfs_data_t BFS;


void
bfs
();

#endif
