#include "ctx.h"
#include "encoding.h"
#include "config.h"
#include "attrs.h"

ctx_t CTX;


void
init_ctx
() {
  memset(&CTX, 0, sizeof(ctx_t));
  CTX.print_ctr = CFG.print_ctr;
  nsec_timer_init(&CTX.timer);
  nsec_timer_start(&CTX.timer);
  CTX.hash = hset_new
    (sizeof(char *),
     CFG.init_hash_size,
     CFG.store_hash_keys,
     encoding_hash,
     encoding_equal);
}


void
free_ctx
() {
  if(CTX.hash) {
    hset_free(CTX.hash);
  }
  if(CTX.out) {
    free(CTX.out);
  }
}


void
ctx_print_statistics
() {
  nsec_timer_stop(&CTX.timer);
  printf("========================================\n\
search terminated = %d\n\
states stored (end) = %d\n\
states stored (peak) = %d\n\
states processed = %d\n\
states dead = %d\n\
arcs = %lu\n\
sec. of search time = %f\n",
         CTX.search_terminated,
         CTX.states,
         CTX.states_peak,
         CTX.states_processed,
         CTX.states_dead,
         CTX.arcs,
         CTX.timer.value / 1000000000.0
  );
  if(CTX.levels) {
    printf("levels = %d\n", CTX.levels);
  }
  if(CTX.hash) {
    printf("bytes for hash table structure = %lu\n", hset_mem_size(CTX.hash));
  }
  printf("========================================\n");
}


void
ctx_output_statistics
() {
  if(CTX.out == NULL) {
    return;
  }
  printf("==%s==\n", CTX.out);
  FILE * f = fopen(CTX.out, "w");
  fprintf(f, "{\n\
   \"terminated\": %d,\n\
   \"states\": %d,\n\
   \"deadlock\": %d,\n\
   \"arcs\": %lu,\n\
   \"time\": %f,\n\
   \"levels\": %d,\n\
   \"bytes_states\": %lu,\n\
}\n",
          CTX.search_terminated,
          CTX.states,
          CTX.states_dead,
          CTX.arcs,
          CTX.timer.value / 1000000000.0,
          CTX.levels,
          hset_mem_size(CTX.hash)
  );
  fclose(f);
}


#define _ctx_encode_marking(m, c)                                       \
  uint16_t size = marking_char_size(m);                                 \
  const uint16_t tot_size = ATTRS.char_size + sizeof(uint16_t) + size;  \
  char * c = malloc(tot_size);                                          \
  memset(c, 0, tot_size);                                               \
  * ((uint16_t *) encoding_pos_size(c)) = size;                         \
  marking_serialise(m, encoding_pos_marking(c), &size);


bool_t
ctx_marking_insert
(marking_t m,
 hset_mdata_t * mdata) {
  _ctx_encode_marking(m, c);
  if(!hset_insert(CTX.hash, &c, mdata)) {
    free(c);
  } else {
    CTX.states ++;
    if(CTX.states > CTX.states_peak) {
      CTX.states_peak = CTX.states;
    }
    return TRUE;
  }
  return FALSE;
}


void
ctx_marking_delete
(hset_ref_t ref) {
  void * m = hset_delete(CTX.hash, ref);
  if(m) {
    CTX.states --;
    free(m);
  }
}


void
ctx_trans_executed
() {
  CTX.arcs ++;
  CTX.print_ctr --;
  if(0 == CTX.print_ctr) {
    CTX.print_ctr = CFG.print_ctr;
    printf("%d states stored, %d states processed\n",
           CTX.states, CTX.states_processed);
  }
}


bool_t
ctx_marking_is_new
(marking_t m) {
  bool_t result;
  _ctx_encode_marking(m, c);
  result = hset_contains(CTX.hash, &c, NULL);
  free(c);
  return !result;
}
