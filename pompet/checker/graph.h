#ifndef LIB_GRAPH
#define LIB_GRAPH

#include "includes.h"
#include "ptnet.h"
#include "hset.h"
#include "common.h"


void
graph_new_marking
(hset_ref_t ref,
 marking_t m);


void
graph_new_edge
(hset_ref_t from,
 trid_t t,
 hset_ref_t to);


void
graph_report
();

#endif
