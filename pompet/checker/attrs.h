#ifndef LIB_ATTRS
#define LIB_ATTRS

#include "includes.h"

#define PROV_ORANGE 0
#define PROV_RED    1
#define PROV_PURPLE 2
#define PROV_GREEN  3

#define NO_ATTRS 8

typedef uint32_t attr_val_t;

typedef enum {
  ATTR_ID,
  ATTR_OPEN,
  ATTR_PROV_COLOR,
  ATTR_PROV_FEXPANDED,
  ATTR_PROV_REVISIT,
  ATTR_PROV_SAFE,
  ATTR_PROG,
  ATTR_PERSISTENT
} attr_marking_t;

typedef struct {
  char attrs[NO_ATTRS];
  int pos[NO_ATTRS];
  int size[NO_ATTRS];
  int char_size;
} attr_data_t;

extern attr_data_t ATTRS;


void
init_attrs
();


void
attr_set
(attr_marking_t attr,
 char * attrs,
 attr_val_t val);


attr_val_t
attr_get
(attr_marking_t attr,
 char * attrs);

#endif
