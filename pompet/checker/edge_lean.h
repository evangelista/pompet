#ifndef LIB_EDGE_LEAN
#define LIB_EDGE_LEAN

#include "includes.h"
#include "common.h"
#include "ptnet.h"


void
edge_lean
(trid_t prev,
 list_t en);

#endif
