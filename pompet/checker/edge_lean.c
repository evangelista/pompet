#include "edge_lean.h"
#include "ptnet.h"


char
_edge_lean_keep_trans
(void * void_t,
 void * void_prev) {
  trid_t t = * ((trid_t *) void_t);
  trid_t prev = * ((trid_t *) void_prev);
  return prev < t || NET.DS[t][prev];
}


void
edge_lean
(trid_t prev,
 list_t en) {
  list_filter(en, _edge_lean_keep_trans, &prev);
}
