#include "config.h"

config_t CFG = {
  .algo = ALGO_DFS,
  .stub_algo = STUB_ALGO_NONE,
  .stub_trans_choice = STUB_TRANS_CHOICE_FST,
  .stub_scapegoat_choice = STUB_SCAPEGOAT_CHOICE_FST,
  .init_hash_size = 8000000,
  .print_ctr = 100000,
  .edge_lean = 0,
  .store_hash_keys = 0,
  .use_units = 0,
  .prov = PROV_NONE,
  .graph_analysis = FALSE
};
