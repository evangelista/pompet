#include "common.h"
#include "stub_closure.h"
#include "config.h"


typedef struct {
  token_t * m;
  bool_t * is_stub;
  bool_t * is_en;
  bool_t * is_fwd;
  uint32_t * incr_p;
  list_t en;
  list_t todo;
} stub_closure_data_t;


void
_stub_closure_check_place
(plid_t p,
 stub_closure_data_t * data);


void
_stub_closure_add_stub
(trid_t t,
 bool_t put_in_todo,
 stub_closure_data_t * data) {
  if(!data->is_stub[t]) {
    data->is_stub[t] = TRUE;
    if(put_in_todo) {
      list_append(data->todo, &t);
    }
    if(CFG.stub_algo == STUB_ALGO_CLODEL ||
       CFG.stub_algo == STUB_ALGO_CLOSURE_OPT) {

      /* decrease incr_p of each output place */
      for(int i = 0; i < NET.up_t_nb[t]; i ++) {
        const arc_t a = NET.up_t[t][i];
        if(a.tok > 0) {
          assert(data->incr_p[a.id] > 0);
          data->incr_p[a.id] --;
          _stub_closure_check_place(a.id, data);
        }
      }
    }
  }
}


/**
 *  (CLOSURE-OPT only) if the incr counter of place p has reached 0 =>
 *  mark all transitions that are disabled by p as stubborn
 */
void
_stub_closure_check_place
(plid_t p,
 stub_closure_data_t * data) {
  if(data->incr_p[p] == 0) {
    for(int i = 0; i < NET.post_p_nb[p]; i ++) {
      const arc_t a = NET.post_p[p][i];
      if(a.tok > data->m[p]) {
        _stub_closure_add_stub(a.id, FALSE, data);
      }
    }
  }
}


/**
 *  count the number of input transitions of place pid that are not
 *  already stubborn
 */
uint32_t
_stub_closure_count_scapegoat_inputs
(trid_t t,
 plid_t p,
 stub_closure_data_t * data) {
  uint32_t result = 0;
  const bool_t fwd_check =
    CFG.stub_scapegoat_choice == STUB_SCAPEGOAT_CHOICE_MINFWD ||
    CFG.stub_scapegoat_choice == STUB_SCAPEGOAT_CHOICE_MAXFWD;
  const bool_t en_check =
    CFG.stub_scapegoat_choice == STUB_SCAPEGOAT_CHOICE_MINEN ||
    CFG.stub_scapegoat_choice == STUB_SCAPEGOAT_CHOICE_MAXEN;

  for(int i = 0; i < NET.add_p_nb[p]; i ++) {
    const trid_t u = NET.add_p[p][i];
    if(!data->is_stub[u]) {
      if(fwd_check) {
        if(data->is_fwd[u]) {
          result += 1000000;
        } else if(data->is_en[u]) {
          result += 1000;
        } else {
          result ++;
        }
      } else if(en_check) {
        if(data->is_en[u]) {
          result += 1000;
        } else {
          result ++;
        }
      } else {
        result ++;
      }
    }
  }
  return result;
}


/**
 *  put in stub all transitions of which the firing increase the
 *  number of tokens in p
 */
void
_stub_closure_handle_scapegoat
(trid_t t,
 plid_t p,
 stub_closure_data_t * data) {
  for(int i = 0; i < NET.add_p_nb[p]; i ++) {
    const trid_t u = NET.add_p[p][i];
    _stub_closure_add_stub(u, TRUE, data);
  }
}


/**
 *  check if t already has a valid scapegoat place that is part of a
 *  unit
 */
bool_t
_stub_closure_has_valid_unit_scapegoat
(trid_t t,
 stub_closure_data_t * data) {
  /* for each input place of t */
  for(int i = 0; i < NET.pre_t_nb[t]; i ++) {
    const plid_t p = NET.pre_t[t][i].id;
    /* if p is not marked and is part of a unit un */
    if(data->m[p] == 0 && NET.unit_p[p] < NET.un_nb) {
      /* find the place q of the unit that is marked and if all
         transitions u that remove tokens from q are stubborn then
         return true */
      const unid_t un = NET.unit_p[p];
      for(int j = 0; j < NET.pl_u_nb[un]; j ++) {
        const plid_t q = NET.pl_u[un][j];
        if(data->m[q] > 0) {
          bool_t all_stub = TRUE;
          for(int k = 0; all_stub && k < NET.rem_p_nb[q]; k ++) {
            const trid_t u = NET.rem_p[q][k];
            all_stub = data->is_stub[u];
          }
          if(all_stub) {
            return TRUE;
          }
        }
      }
    }
  }
  return FALSE;
}

/**
 *  STUB closure algorithm main function
 */
void
stub_closure
(list_t en,
 void * data,
 token_t * m,
 bool_t * is_en,
 bool_t * is_fwd,
 bool_t * is_stub) {
  plid_t p;
  uint32_t sg_ctr, ctr, no_sgs, sgs[NET.pl_nb];
  heap_t h;
  list_iter_t it;
  bool_t best_stub[NET.tr_nb];
  int start, i, sg, min_cost, cost = 0;
  trid_t t, u = 0;
  bool_t first = TRUE;
  stub_data_t * stub_data = (stub_data_t *) data;
  list_t included = stub_data ? stub_data->included : NULL;
  uint32_t incr_p[NET.pl_nb];
  stub_trans_choice_t tc = CFG.stub_trans_choice;
  stub_closure_data_t cdata = {
    .m = m,
    .incr_p = incr_p,
    .en = en,
    .is_en = is_en,
    .is_fwd = is_fwd,
    .is_stub = is_stub
  };

  h = local_heap_new();
  if(included) {
    start = 0;
    tc = STUB_TRANS_CHOICE_FST;
  } else {
    if(STUB_TRANS_CHOICE_RND == tc) {
      start = ((uint32_t) rand()) % list_size(en);
    } else {
      start = 0;
    }
  }
 start_stubborn_computation:
  cdata.todo = list_new(h, sizeof(trid_t));
  memset(cdata.is_stub, 0, sizeof(bool_t) * NET.tr_nb);
  memcpy(incr_p, NET.add_p_nb, sizeof(uint32_t) * NET.pl_nb);
  for(i = 0; i < NET.pl_nb; i ++) {
    _stub_closure_check_place(i, &cdata);
  }
  if(included) {
    list_for_each(included, it) {
      u = * ((trid_t *) list_iter_item(it));
      if(cdata.is_en[u]) {
        _stub_closure_add_stub(u, TRUE, &cdata);
      }
    }
  } else {
    u = * ((trid_t *) list_nth(en, start));
    _stub_closure_add_stub(u, TRUE, &cdata);
  }
  while(!list_is_empty(cdata.todo)) {
    list_pick_first(cdata.todo, &t);
    if(cdata.is_en[t]) {

      /* enabled transition handling */
      for(i = 0; i < NET.conf_t_nb[t]; i ++) {
        if(!NET.DF || NET.DF[t][NET.conf_t[t][i]]) {
          _stub_closure_add_stub(NET.conf_t[t][i], TRUE, &cdata);
        }
      }

    } else {

      if(CFG.use_units && _stub_closure_has_valid_unit_scapegoat(t, &cdata)) {
        continue;
      }

      /* disabled transition handling */
      for(no_sgs = 0, sg_ctr = 0, sg = - 1, i = 0;
	  i < NET.pre_t_nb[t];
	  i ++) {
	p = NET.pre_t[t][i].id;

	/* a scapegoat has been found */
	if(NET.pre_t[t][i].tok > m[p]) {
          
	  if(CFG.stub_scapegoat_choice == STUB_SCAPEGOAT_CHOICE_RND) {
	    sgs[no_sgs] = p;
	    no_sgs ++;
	  } else if(-1 == sg) {
            sg = p;
	    if(CFG.stub_scapegoat_choice == STUB_SCAPEGOAT_CHOICE_FST) {
	      break;
	    }
	    sg_ctr = _stub_closure_count_scapegoat_inputs(t, p, &cdata);
          } else {
	    ctr = _stub_closure_count_scapegoat_inputs(t, p, &cdata);
	    switch(CFG.stub_scapegoat_choice) {
	    case STUB_SCAPEGOAT_CHOICE_MIN:
	    case STUB_SCAPEGOAT_CHOICE_MINEN:
	    case STUB_SCAPEGOAT_CHOICE_MINFWD:
	      if(ctr < sg_ctr) {
		sg = p;
		sg_ctr = ctr;
	      }
	      break;
	    case STUB_SCAPEGOAT_CHOICE_MAX:
	    case STUB_SCAPEGOAT_CHOICE_MAXEN:
	    case STUB_SCAPEGOAT_CHOICE_MAXFWD:
	      if(ctr > sg_ctr) {
		sg = p;
		sg_ctr = ctr;
	      }
	      break;
	    default:
	      assert(0);
	    }
          }
        }
      }
      if(CFG.stub_scapegoat_choice == STUB_SCAPEGOAT_CHOICE_RND) {
	sg = sgs[((uint32_t) rand()) % no_sgs];
      }
      assert(sg >= 0);
      _stub_closure_handle_scapegoat(t, sg, &cdata);
    }
  }

  /**
   *  strategy MIN and MINFWD: count the number of enabled/forward
   *  transitions
   */
  if(STUB_TRANS_CHOICE_MINFWD == tc ||
     STUB_TRANS_CHOICE_MIN == tc) {
    cost = 0;
    for(t = 0; t < NET.tr_nb; t ++) {
      if(cdata.is_stub[t] && cdata.is_en[t]) {
        if(STUB_TRANS_CHOICE_MINFWD == tc) {
          cost += is_fwd[t];
        } else {
          cost ++;
        }
      }
    }
  }

  /**
   *  check what to do with the scapegoat computed
   */
  if(first) {
    first = FALSE;
    memcpy(best_stub, cdata.is_stub, sizeof(bool_t) * NET.tr_nb);
    min_cost = cost;
  } else {
    assert(tc == STUB_TRANS_CHOICE_MINFWD || tc == STUB_TRANS_CHOICE_MIN);
    if(cost < min_cost) {
      memcpy(best_stub, cdata.is_stub, sizeof(bool_t) * NET.tr_nb);
      min_cost = cost;
    }
  }

  /**
   *  under some conditions we have to compute another stubborn set
   *  with a different starting transition
   */
  if(!included && start < list_size(en) - 1 &&
     ((STUB_TRANS_CHOICE_MIN == tc && 1 != min_cost)
      ||
      (STUB_TRANS_CHOICE_MINFWD == tc && 0 != min_cost))) {
    start ++;
    goto start_stubborn_computation;
  }

  memcpy(is_stub, best_stub, sizeof(bool_t) * NET.tr_nb);
  heap_free(h);
}
