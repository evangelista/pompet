#ifndef LIB_ENCODING
#define LIB_ENCODING

#include "includes.h"
#include "common.h"
#include "hset.h"


typedef char * encoding_t;


#define encoding_pos_attrs(e) \
  (e)
#define encoding_pos_size(e) \
  (e + ATTRS.char_size)
#define encoding_pos_marking(e) \
  (e + ATTRS.char_size + sizeof(uint16_t))
#define encoding_get(ref)                       \
  (* ((char **) hset_get_ptr(CTX.hash, ref)))
#define encoding_attr_set(attr, e, val)         \
  attr_set(attr, encoding_pos_attrs(e), val);
#define encoding_attr_get(attr, e)              \
  attr_get(attr, encoding_pos_attrs(e))


hval_t
encoding_hash
(void * e);


char
encoding_equal
(void * e1,
 void * e2);

#endif
