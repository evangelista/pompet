#ifndef LIB_LIST
#define LIB_LIST

#include "includes.h"
#include "heap.h"

typedef uint32_t list_index_t;

typedef uint32_t list_size_t;

typedef struct struct_list_t * list_t;

typedef struct struct_list_node_t * list_iter_t;

typedef char (* list_pred_func_t) (void *, void *);
typedef void (* list_app_func_t) (void *, void *);
typedef int (* list_item_cmp_func_t) (void *, void *);


list_t
list_new
(heap_t heap,
 uint32_t sizeof_item);

char
list_is_empty
(list_t list);

list_size_t
list_size
(list_t list);

void
list_free
(list_t list);

void
list_reset
(list_t list);

list_t
list_copy
(list_t list,
 heap_t heap);

void *
list_first
(list_t list);

void *
list_last
(list_t list);

void *
list_nth
(list_t list,
 list_index_t n);

void
list_app
(list_t list,
 list_app_func_t app_func,
 void * data);

void
list_prepend
(list_t list,
 void * item);

void
list_append
(list_t list,
 void * item);

void
list_insert_sorted
(list_t list,
 void * item,
 list_item_cmp_func_t item_cmp_func);

void
list_pick_last
(list_t list,
 void * item);

void
list_pick_first
(list_t list,
 void * item);

void
list_pick_nth
(list_t list,
 list_index_t n,
 void * item);

void *
list_find
(list_t list,
 list_pred_func_t pred_func,
 void * find_data);

void *
list_find_from
(list_t list,
 list_index_t from,
 list_pred_func_t pred_func,
 void * find_data);

void
list_filter
(list_t list,
 list_pred_func_t pred_func,
 void * filter_data);

list_iter_t
list_get_iter
(list_t list);

list_iter_t
list_iter_next
(list_iter_t it);

char
list_iter_at_end
(list_iter_t it);

void *
list_iter_item
(list_iter_t it);


#define list_for_each(list, it)                 \
  for(it = list_get_iter(list);                 \
      !list_iter_at_end(it);                    \
      it = list_iter_next(it))

#endif
