#include "encoding.h"
#include "attrs.h"


hval_t
encoding_hash
(void * e) {
  char * c = * ((char **) e);
  return hset_default_hash(encoding_pos_marking(c),
                           * ((uint16_t *) encoding_pos_size(c)));
}


char
encoding_equal
(void * e1,
 void * e2) {
  char * c1 = * ((char **) e1);
  char * c2 = * ((char **) e2);
  const uint16_t s1 = * ((uint16_t *) encoding_pos_size(c1));
  const uint16_t s2 = * ((uint16_t *) encoding_pos_size(c2));
  return s1 == s2 && !memcmp(encoding_pos_marking(c1),
                             encoding_pos_marking(c2), s1);
}
