#ifndef LIB_HSET
#define LIB_HSET

#include "stdint.h"
#include "stdlib.h"
#include "string.h"

typedef struct struct_hset_t * hset_t;

typedef uint64_t hval_t;

typedef char (* hset_cmp_func_t) (void *, void *);

typedef hval_t (* hset_hash_func_t) (void *);

typedef uint64_t hset_ref_t;

typedef struct {
  hval_t hash;  /* hash value computed for the item */
  hset_ref_t ref;  /* reference to the item in the set */
} hset_mdata_t;


hval_t
hset_default_hash
(char * v,
 unsigned int len);


hset_t
hset_new
(uint32_t sizeof_item,
 uint32_t base_size,
 char store_hash,
 hset_hash_func_t hash_func,
 hset_cmp_func_t cmp_func);


void
hset_free
(hset_t hset);


void
hset_empty
(hset_t hset);


uint32_t
hset_num_items
(hset_t hset);


size_t
hset_mem_size
(hset_t hset);


void
hset_set_hash_size
(hset_t hset,
 uint32_t hash_size);


char
hset_insert
(hset_t hset,
 void * item,
 hset_mdata_t * mdata);


void *
hset_delete
(hset_t hset,
 hset_ref_t ref);


char
hset_contains
(hset_t hset,
 void * item,
 hset_mdata_t * mdata);


char
hset_get
(hset_t hset,
 hset_ref_t ref,
 void * item);


void *
hset_get_ptr
(hset_t hset,
 hset_ref_t ref);


void
hset_fold
(hset_t hset,
 void (* fold_func) (void *, void *),
 void * data);


void
hset_hash_fold
(hset_t hset,
 hval_t h,
 void (* fold_func) (hval_t, void *, void *),
 void * data);

#endif
