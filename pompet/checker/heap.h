#ifndef LIB_HEAP
#define LIB_HEAP

#include "common.h"

#define SYSTEM_HEAP NULL
#define LOCAL_HEAP  0

void
init_heap
();

typedef uint64_t mem_size_t;

typedef void * heap_t;

heap_t
local_heap_new
();

void *
mem_alloc
(heap_t heap,
 mem_size_t size);

void *
mem_alloc0
(heap_t heap,
 mem_size_t size);

void
mem_free
(heap_t heap,
 void * ptr);

void
heap_reset
(heap_t heap);

void
heap_free
(heap_t heap);

mem_size_t
heap_get_position
(heap_t heap);

void
heap_set_position
(heap_t heap,
 mem_size_t pos);

bool_t
heap_has_mem_free
(heap_t heap);

mem_size_t
heap_size
(heap_t heap);

#endif
