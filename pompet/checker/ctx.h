#ifndef LIB_CTX
#define LIB_CTX

#include "includes.h"
#include "common.h"
#include "hset.h"
#include "heap.h"
#include "ptnet.h"


typedef struct {
  uint64_t arcs;
  uint32_t states;
  uint32_t states_dead;
  uint32_t states_processed;
  uint32_t states_peak;
  uint32_t levels;
  uint32_t print_ctr;
  nsec_timer_t timer;
  char search_terminated;
  hset_t hash;
  char * out;
} ctx_t;

extern ctx_t CTX;


void
init_ctx
();


void
free_ctx
();


void
ctx_print_statistics
();

void
ctx_output_statistics
();


bool_t
ctx_marking_insert
(marking_t m,
 hset_mdata_t * mdata);


void
ctx_marking_delete
(hset_ref_t ref);


bool_t
ctx_marking_is_new
(marking_t m);

void
ctx_trans_executed
();

#endif
