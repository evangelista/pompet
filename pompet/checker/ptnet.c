#include "ptnet.h"
#include "ptnet_loader.h"
#include "config.h"
#include "stub_closure.h"
#include "stub_deletion.h"

#define PTNET_TOKENS_WIDTH_WIDTH 5
#define PTNET_PRE(_p, _t) NET.inc_pre[(_t) + (_p) * NET.tr_nb]
#define PTNET_POST(_p, _t) NET.inc_post[(_t) + (_p) * NET.tr_nb]
#define PTNET_FOR_PID(_pid) for(uint32_t _pid = 0; _pid < NET.pl_nb; _pid ++)
#define PTNET_FOR_TID(_tid) for(uint32_t _tid = 0; _tid < NET.tr_nb; _tid ++)
#define PTNET_MIN(_a, _b) (((_a) < (_b)) ? (_a) : (_b))

#define _marking_to_int_vector(marking, vector) {                       \
    list_iter_t iter;                                                   \
    memset(vector, 0, sizeof(token_t) * NET.pl_nb);                     \
    list_for_each(marking->marked, iter) {                              \
      marked_place_t * mp = (marked_place_t *) list_iter_item(iter);    \
      vector[mp->pid] = mp->tok;                                        \
    }                                                                   \
  }

ptnet_t NET;


void
init_ptnet
() {
  ptnet_load(&NET);
  ptnet_load_post();
}


/* marking related functions */
marking_t
marking_initial
(heap_t heap) {
  return marking_copy(NET.m0, heap);
}


marking_t
marking_copy
(marking_t m,
 heap_t heap) {
  marking_t result = mem_alloc(heap, sizeof(struct_marking_t));
  result->marked = list_copy(m->marked, heap);
  result->heap = heap;
  return result;
}


void
marking_free
(marking_t m) {
  list_free(m->marked);
  mem_free(m->heap, m);
}


list_t
marking_enabled
(marking_t m,
 heap_t heap) {
  list_t result = list_new(heap, sizeof(trans_t));
  list_iter_t it;
  trans_t t;
  uint32_t nb_pre_t[NET.tr_nb];
  int i;
  arc_t arc;

  //  TODO: special case for transitions that have no input place
  memcpy(nb_pre_t, NET.pre_t_nb, sizeof(uint32_t) * NET.tr_nb);
  list_for_each(m->marked, it) {
    marked_place_t * p = (marked_place_t *) list_iter_item(it);
    for(i = 0; i < NET.post_p_nb[p->pid]; i ++) {
      arc = NET.post_p[p->pid][i];
      if(p->tok >= arc.tok) {
	nb_pre_t[arc.id] --;
	if(0 == nb_pre_t[arc.id]) {
	  t = arc.id;
	  list_append(result, &t);
	}
      }
    }
  }
  return result;
}


void
marking_print
(marking_t m,
 FILE * out) {
  list_iter_t it;
  fprintf(out, "{\n");
  list_for_each(m->marked, it) {
    marked_place_t * p = (marked_place_t *) list_iter_item(it);
    fprintf(out, "   %s = %d\n", NET.pl_names[p->pid], p->tok);
  }
  fprintf(out, "}\n");
}


char
_marking_tokens_width
(token_t tok) {
  char result = 0;
  while(tok) {
    tok = tok >> 1;
    result ++;
  }
  assert(result <= 32);
  return result;
}


unsigned int
marking_char_size
(marking_t m) {
  list_iter_t it;
  unsigned int result = NET.pl_nb_width;
  list_for_each(m->marked, it) {
    marked_place_t * p = (marked_place_t *) list_iter_item(it);
    result += NET.pl_id_width + 1;
    if(p->tok > 1) {
      result += PTNET_TOKENS_WIDTH_WIDTH + _marking_tokens_width(p->tok);
    }
  }
  return (result / CHAR_BIT) + ((result % CHAR_BIT) ? 1 : 0);
}


void
marking_serialise
(marking_t m,
 char * v,
 uint16_t * size) {
  const int len = list_size(m->marked);
  list_iter_t it;
  bit_stream_t stream;
  if(0 == *size) {
    *size = marking_char_size(m);
  }
  v[*size - 1] = 0; 
  bit_stream_init(stream, v);
  bit_stream_set(stream, len, NET.pl_nb_width);
  list_for_each(m->marked, it) {
    const marked_place_t * p = (marked_place_t *) list_iter_item(it);
    bit_stream_set(stream, p->pid, NET.pl_id_width);
    if(1 == p->tok) {
      bit_stream_set(stream, 0, 1);
    } else {
      const char w = _marking_tokens_width(p->tok);
      bit_stream_set(stream, 1, 1);
      bit_stream_set(stream, w, PTNET_TOKENS_WIDTH_WIDTH);
      bit_stream_set(stream, p->tok, (int) w);
    }
  }
}


marking_t
marking_unserialise
(char * v,
 heap_t heap) {
  int len;
  bit_stream_t stream;
  marking_t result = mem_alloc(heap, sizeof(struct_marking_t));
  char w;
  result->heap = heap;
  result->marked = list_new(heap, sizeof(marked_place_t));
  bit_stream_init(stream, v);
  bit_stream_get(stream, len, NET.pl_nb_width);
  while(len --) {
    char c;
    marked_place_t p;
    bit_stream_get(stream, p.pid, NET.pl_id_width);
    bit_stream_get(stream, c, 1);
    if(c) {
      bit_stream_get(stream, w, PTNET_TOKENS_WIDTH_WIDTH);
      bit_stream_get(stream, p.tok, (int) w);
    } else {
      p.tok = 1;
    }      
    list_append(result->marked, &p);
  }
  return result;
}


char
_marking_stub_is_stubborn
(void * t,
 void * stub) {
  return ((char *) stub)[* ((trid_t *) t)];
}


void
marking_stub
(marking_t m,
 list_t en,
 stub_data_t * data) {
  list_iter_t it;
  token_t toks[NET.pl_nb];
  bool_t check_fwd, is_en[NET.tr_nb], is_fwd[NET.tr_nb], is_stub[NET.tr_nb];
  const stub_trans_choice_t tr_choice = CFG.stub_trans_choice;
  const stub_scapegoat_choice_t sg_choice = CFG.stub_scapegoat_choice;

  /* no enabled transition => nothing to do */
  if(list_is_empty(en)) {
    return;
  }

  /* put the marking in an int vector for easier manipulation */
  _marking_to_int_vector(m, toks);

  /* same for the enabled list we put in a bool vector */
  memset(is_en, 0, NET.tr_nb * sizeof(bool_t));
  list_for_each(en, it) {
    trid_t t = * ((trid_t *) list_iter_item(it));
    is_en[t] = TRUE;
  }

  /* with some strategies we have to compute forward transitions */
  check_fwd = tr_choice == STUB_TRANS_CHOICE_MAXFWD
    || tr_choice == STUB_TRANS_CHOICE_MINFWD
    || sg_choice == STUB_SCAPEGOAT_CHOICE_MAXFWD
    || sg_choice == STUB_SCAPEGOAT_CHOICE_MINFWD;
  if(check_fwd && data->is_new_func) {
    memset(is_fwd, 0, NET.tr_nb * sizeof(bool_t));
    list_for_each(en, it) {
      trid_t t = * ((trid_t *) list_iter_item(it));
      trans_exec(t, m);
      is_fwd[t] = data->is_new_func(m);
      trans_undo(t, m);
    }
  }

  /* call the appropriate algorithm */
  switch(CFG.stub_algo) {
  case STUB_ALGO_CLOSURE:
  case STUB_ALGO_CLOSURE_OPT:
    stub_closure(en, data, toks, is_en, is_fwd, is_stub);
    break;
  case STUB_ALGO_CLODEL:
    stub_clodel(en, data, toks, is_en, is_fwd, is_stub);
    break;
  case STUB_ALGO_DELETION:
    stub_deletion(en, data, toks, is_en, is_fwd, is_stub);
    break;
  case STUB_ALGO_NONE:
    assert(0);
  }
  list_filter(en, _marking_stub_is_stubborn, is_stub);
}


/* transition related functions */
order_t
trans_cmp
(trans_t t,
 trans_t u) {
  if(t < u) {
    return LESS;
  } else if(t > u) {
    return GREATER; 
  } else {
    return EQUAL;
  }
}


char
_marked_place_is_not_empty
(void * item,
 void * data) {
  marked_place_t * p = (marked_place_t *) item;
  return (0 != p->tok) ? TRUE : FALSE;
}


int
_marked_place_cmp
(void * a,
 void * b) {
  marked_place_t * pa = (marked_place_t *) a;
  marked_place_t * pb = (marked_place_t *) b;
  if(pa->pid < pb->pid) {
    return -1;
  } else if(pa->pid > pb->pid) {
    return 1;
  } else {
    return 0;
  }
}


void
trans_exec_or_undo
(trans_t t,
 marking_t m,
 char op) {
  int i = 0;
  list_iter_t it = list_get_iter(m->marked);
  arc_t arc;
  bool_t done[NET.up_t_nb[t]];
  memset(done, 0, sizeof(bool_t) * NET.up_t_nb[t]);
  i = 0;
  while(!list_iter_at_end(it) && i < NET.up_t_nb[t]) {
    marked_place_t * p = (marked_place_t *) list_iter_item(it);
    arc = NET.up_t[t][i];
    if(p->pid == arc.id) {
      p->tok += op * arc.tok;
      done[i] = TRUE;
      it = list_iter_next(it);
      i ++;
    } else if(p->pid < arc.id) {
      it = list_iter_next(it);
    } else {
      i ++;
    }
  }
  for(i = 0; i < NET.up_t_nb[t]; i++) {
    arc = NET.up_t[t][i];
    if(!done[i]) {
      marked_place_t new_m = { .pid = arc.id, .tok = 0 };
      new_m.tok += op * arc.tok;
      list_insert_sorted(m->marked, &new_m, _marked_place_cmp);
    }
  }
  list_filter(m->marked, _marked_place_is_not_empty, NULL);
}


void
trans_exec
(trans_t t,
 marking_t m) {
  trans_exec_or_undo(t, m, 1);
}


void
trans_undo
(trans_t t,
 marking_t m) {
  trans_exec_or_undo(t, m, -1);
}


void
trans_print
(trans_t t,
 FILE * out) {
  fprintf(out, "%s\n", NET.tr_names[t]);
}


/* net related functions */
uint32_t *
_ptnet_read_inc
(char * inc_str) {
  const char delim [] = ";";
  const int len = strlen(inc_str);
  uint32_t pid, tid, val;
  uint32_t * result = malloc(NET.pl_nb * NET.tr_nb * sizeof(uint32_t));
  char * ptr, * str = malloc(len + 1);

  memcpy(str, inc_str, len + 1);
  ptr = strtok(str, delim);
  memset(result, 0, NET.pl_nb * NET.tr_nb * sizeof(uint32_t));
  while(ptr != NULL) {
    sscanf(ptr, "%d", &pid);
    ptr = strtok(NULL, delim);
    sscanf(ptr, "%d", &tid);
    ptr = strtok(NULL, delim);
    sscanf(ptr, "%d", &val);
    result[tid + pid * NET.tr_nb] = val;
    ptr = strtok(NULL, delim);
  }
  free(str);
  return result;
}

marking_t
_ptnet_read_m0
() {
  const char delim [] = ";";
  const int len = strlen(NET.m0_str);
  char * ptr, * str = malloc(len + 1);
  marking_t result = malloc(sizeof(struct_marking_t));

  memcpy(str, NET.m0_str, len + 1);
  result->heap = NULL;
  result->marked = list_new(NULL, sizeof(marked_place_t));
  ptr = strtok(str, delim);
  PTNET_FOR_PID(pid) {
    uint32_t val;
    sscanf(ptr, "%d", &val);
    if(val > 0) {
      marked_place_t m = { .pid = pid, .tok = val};
      list_append(result->marked, &m);
    }
    ptr = strtok(NULL, delim);
  }
  assert(!ptr);
  free(str);
  return result;
}

void
ptnet_load_post
() {
#define _ptnet_load_post_init_array(_arr_nb, _arr, _nb, _t) {     \
    NET._arr_nb = malloc(NET._nb * sizeof(uint32_t));           \
    NET._arr = malloc(NET._nb * sizeof(_t *));                  \
  }
#define _ptnet_load_post_init_slot(_arr_nb, _arr, _id, _no, _t) { \
    NET._arr_nb[_id] = _no;                                     \
    NET._arr[_id] = malloc(sizeof(_t) * _no);                   \
  }
#define _ptnet_load_post_conf(_tid, _uid) conf[(_tid) * NET.tr_nb + _uid]
  uint32_t i;
  bool_t * conf = malloc(NET.tr_nb * NET.tr_nb * sizeof(bool_t));

  memset(conf, 0, NET.tr_nb * NET.tr_nb * sizeof(bool_t));

  NET.inc_pre = _ptnet_read_inc(NET.inc_pre_str);
  NET.inc_post = _ptnet_read_inc(NET.inc_post_str);
  NET.m0 = _ptnet_read_m0();

  _ptnet_load_post_init_array(add_p_nb, add_p, pl_nb, trid_t);
  _ptnet_load_post_init_array(rem_p_nb, rem_p, pl_nb, trid_t);
  _ptnet_load_post_init_array(post_p_nb, post_p, pl_nb, arc_t);
  _ptnet_load_post_init_array(conf_t_nb, conf_t, tr_nb, trid_t);
  _ptnet_load_post_init_array(pre_t_nb, pre_t, tr_nb, arc_t);
  _ptnet_load_post_init_array(up_t_nb, up_t, tr_nb, arc_t);

  /* places */
  PTNET_FOR_PID(pid) {

    uint32_t no_post = 0, no_add = 0, no_rem = 0;
    PTNET_FOR_TID(tid) {
      if(PTNET_PRE(pid, tid) > 0) {
        no_post ++;
      }
      if(PTNET_POST(pid, tid) > PTNET_PRE(pid, tid)) {
        no_add ++;
      }
      if(PTNET_POST(pid, tid) < PTNET_PRE(pid, tid)) {
        no_rem ++;
      }
    }

    /* post_p */
    i = 0;
    _ptnet_load_post_init_slot(post_p_nb, post_p, pid, no_post, arc_t);
    PTNET_FOR_TID(tid) {
      if(PTNET_PRE(pid, tid) > 0) {
        NET.post_p[pid][i] = (arc_t) { .id = tid, .tok = PTNET_PRE(pid, tid)};
        i ++;
      }
    }

    /* add_p */
    i = 0;
    _ptnet_load_post_init_slot(add_p_nb, add_p, pid, no_add, trid_t);
    PTNET_FOR_TID(tid) {
      if(PTNET_POST(pid, tid) > PTNET_PRE(pid, tid)) {
        NET.add_p[pid][i] = (trid_t) tid;
        i ++;
      }
    }

    /* rem_p */
    i = 0;
    _ptnet_load_post_init_slot(rem_p_nb, rem_p, pid, no_rem, trid_t);
    PTNET_FOR_TID(tid) {
      if(PTNET_PRE(pid, tid) > PTNET_POST(pid, tid)) {
        NET.rem_p[pid][i] = (trid_t) tid;
        i ++;
      }
    }
  }

  /* transitions */
  PTNET_FOR_TID(tid) {

    uint32_t no_up = 0, no_pre = 0, no_conf = 0;
    PTNET_FOR_PID(pid) {
      if(PTNET_PRE(pid, tid) != PTNET_POST(pid, tid)) {
        no_up ++;
      }
      if(PTNET_PRE(pid, tid) > 0) {
        no_pre ++;
        PTNET_FOR_TID(uid) {
          if(uid != tid
             && !_ptnet_load_post_conf(tid, uid)
             && (PTNET_MIN(PTNET_POST(pid, tid), PTNET_POST(pid, uid)) <
                 PTNET_MIN(PTNET_PRE(pid, tid), PTNET_PRE(pid, uid)))) {
            no_conf ++;
            _ptnet_load_post_conf(tid, uid) = 1;
          }
        }
      }
    }

    /* up_t */
    i = 0;
    _ptnet_load_post_init_slot(up_t_nb, up_t, tid, no_up, arc_t);
    PTNET_FOR_PID(pid) {
      if(PTNET_PRE(pid, tid) != PTNET_POST(pid, tid)) {
        uint32_t val = PTNET_POST(pid, tid) - PTNET_PRE(pid, tid);
        NET.up_t[tid][i] = (arc_t) { .id = pid, .tok = val};
        i ++;
      }
    }

    /* pre_t */
    i = 0;
    _ptnet_load_post_init_slot(pre_t_nb, pre_t, tid, no_pre, arc_t);
    PTNET_FOR_PID(pid) {
      if(PTNET_PRE(pid, tid) > 0) {
        NET.pre_t[tid][i] = (arc_t) { .id = pid, .tok = PTNET_PRE(pid, tid)};
        i ++;
      }
    }

    /* conf_t */
    i = 0;
    _ptnet_load_post_init_slot(conf_t_nb, conf_t, tid, no_conf, trid_t);
    PTNET_FOR_TID(uid) {
      if(_ptnet_load_post_conf(tid, uid)) {
        NET.conf_t[tid][i] = (trid_t) uid;
        i ++;
      }
    }
  }

  /* dependency graph for deletion algorithm */
  const uint32_t nodes_nb = NET.pl_nb + NET.tr_nb;
  del_graph_node_t node;
  NET.del_graph.nodes_nb = nodes_nb;
  NET.del_graph.deleted = malloc(sizeof(char) * nodes_nb);
  NET.del_graph.protected = malloc(sizeof(char) * nodes_nb);
  NET.del_graph.ref = malloc(sizeof(uint16_t) * nodes_nb);
  NET.del_graph.nodes = malloc(sizeof(del_graph_node_t) * nodes_nb);

  /* predecessors in the dep-graph of a place p: transitions that
     consume tokens from p */
  node.ntype = AND_NODE;
  node.ctype = PLACE_NODE;
  PTNET_FOR_PID(pid) {
    node.pred = list_new(NULL, sizeof(uint32_t));
    PTNET_FOR_TID(tid) {
      if(PTNET_PRE(pid, tid) > 0) {
        list_append(node.pred, &tid);
      }
    }
    NET.del_graph.nodes[pid + NET.tr_nb] = node;
  }

  /* predecessors in the dep-graph of a transition t: (1) transitions
     in conflict with t ; and (2) places of which the firing of t
     increase the marking. */
  node.ntype = OR_NODE;
  node.ctype = TRANS_NODE;
  PTNET_FOR_TID(tid) {
    node.pred = list_new(NULL, sizeof(uint32_t));
    /* (1) */
    PTNET_FOR_TID(uid) {
      if(_ptnet_load_post_conf(tid, uid)) {
        list_append(node.pred, &uid);
      }
    }
    /* (2) */
    PTNET_FOR_PID(pid) {
      if(PTNET_POST(pid, tid) > PTNET_PRE(pid, tid)) {
        uint32_t id = pid + NET.tr_nb;
        list_append(node.pred, &id);
      }
    }
    NET.del_graph.nodes[tid] = node;
  }

  free(conf);
}
