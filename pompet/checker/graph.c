#include "graph.h"
#include "heap.h"


typedef struct {
  char initialised;
  uint32_t * bounds;
  char * firable;
} graph_data_t;

graph_data_t GRAPH = {
  .initialised = 0
};


void
graph_new_marking
(hset_ref_t ref,
 marking_t m) {
  list_iter_t it;
  if(!GRAPH.initialised) {
    GRAPH.initialised = 1;
    GRAPH.bounds = mem_alloc0(NULL, sizeof(uint32_t) * NET.pl_nb);
    GRAPH.firable = mem_alloc0(NULL, NET.tr_nb);
  }
  list_for_each(m->marked, it) {
    marked_place_t mp = * ((marked_place_t *) list_iter_item(it));
    if(GRAPH.bounds[mp.pid] < mp.tok) {
      GRAPH.bounds[mp.pid] = mp.tok;
    }
  }
}


void
graph_new_edge
(hset_ref_t from,
 trid_t t,
 hset_ref_t to) {
  GRAPH.firable[t] = 1;
}


void
graph_report
() {
  printf("*** place bounds ***\n");
  for(int i = 0; i < NET.pl_nb; i ++) {
    printf("%s = %d\n", NET.pl_names[i], GRAPH.bounds[i]);
  }
  printf("\n*** transition firability ***\n");
  for(int i = 0; i < NET.tr_nb; i ++) {
    printf("%s = %d\n", NET.tr_names[i], GRAPH.firable[i]);
  }
}
