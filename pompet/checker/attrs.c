#include "attrs.h"
#include "config.h"


attr_data_t ATTRS;


void
init_attrs
() {
  int pos = 0, size = 0;
  char included = 0;
  memset(&ATTRS, 0, sizeof(attr_data_t));
  for(attr_marking_t a = 0; a < NO_ATTRS; a ++) {
    included = 0;
    switch(a) {
    case ATTR_OPEN:
      size = 1;
      included = 1;
      break;
    case ATTR_PROV_COLOR:
      size = 2;
      included = CFG.algo == ALGO_DFS
        && (CFG.prov == PROV_DST || CFG.prov == PROV_SRC);
      break;
    case ATTR_PROV_FEXPANDED:
      size = 32;
      included = CFG.algo == ALGO_DFS && CFG.prov == PROV_SRC;
      break;
    case ATTR_PROV_REVISIT:
      size = 1;
      included = CFG.algo == ALGO_DFS && CFG.prov == PROV_DST;
      break;
    case ATTR_PROV_SAFE:
      size = 1;
      included = CFG.algo == ALGO_BFS && CFG.prov != PROV_NONE;
      break;
    case ATTR_ID:
      size = 32;
      included = CFG.algo == ALGO_BFS && CFG.prov == PROV_WOPT;
      break;
    case ATTR_PROG:
      size = sizeof(prog_t) * CHAR_BIT;
      included = CFG.algo == ALGO_SWEEP;
      break;
    case ATTR_PERSISTENT:
      size = 1;
      included = CFG.algo == ALGO_SWEEP;
      break;
    }
    ATTRS.attrs[a] = included;
    ATTRS.size[a] = size;
    if(included) {
      ATTRS.pos[a] = pos;
      pos += size;
    }
  }
  ATTRS.char_size = (pos / CHAR_BIT) + ((pos % CHAR_BIT) ? 1 : 0);
}


void
attr_set
(attr_marking_t attr,
 char * attrs,
 attr_val_t val) {
  bit_stream_t stream;
  assert(ATTRS.attrs[attr]);
  bit_stream_init(stream, attrs);
  bit_stream_move(stream, ATTRS.pos[attr]);
  bit_stream_set(stream, val, ATTRS.size[attr]);
}



attr_val_t
attr_get
(attr_marking_t attr,
 char * attrs) {
  attr_val_t result;
  bit_stream_t stream;
  assert(ATTRS.attrs[attr]);
  bit_stream_init(stream, attrs);
  bit_stream_move(stream, ATTRS.pos[attr]);
  bit_stream_get(stream, result, ATTRS.size[attr]);
  return result;
}
