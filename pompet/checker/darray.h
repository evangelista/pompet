#ifndef LIB_DARRAY
#define LIB_DARRAY

#include "heap.h"

typedef uint32_t darray_size_t;

typedef uint32_t darray_index_t;

typedef struct struct_darray_t * darray_t;


darray_t
darray_new
(heap_t heap,
 uint32_t sizeof_item);

void
darray_free
(darray_t darray);

darray_size_t
darray_size
(darray_t darray);

void
darray_reset
(darray_t darray);

uint32_t
darray_sizeof_item
(darray_t darray);

void
darray_push
(darray_t darray,
 void * item);

void
darray_pop
(darray_t darray,
 void * item);

void
darray_top
(darray_t darray,
 void * item);

void *
darray_top_ptr
(darray_t darray);

void
darray_get
(darray_t darray,
 darray_index_t i,
 void * item);

void *
darray_get_ptr
(darray_t darray,
 darray_index_t i);

void
darray_set
(darray_t darray,
 darray_index_t i,
 void * item);  
  
#endif
