#include "prov.h"
#include "ctx.h"
#include "encoding.h"

void
prov_dfs_fully_expand_top
() {
  int i = 0;
  list_iter_t it;
  list_t en = marking_enabled(DFS.now, DFS.heap);
  dfs_stack_item_t * item = darray_top_ptr(DFS.stack);
  encoding_t m = encoding_get(item->ref);
  CTX.arcs -= item->no_en;
  item->cur = 0;
  item->no_en = list_size(en);
  item->en = mem_alloc(DFS.stack_heap, sizeof(trid_t) * item->no_en);
  list_for_each(en, it) {
    item->en[i ++] = * ((trid_t *) list_iter_item(it));
  }
  encoding_attr_set(ATTR_PROV_COLOR, m, PROV_GREEN);
}


/*****************************************************************************
 *  DST PROVISO IN DFS
 *****************************************************************************/

/* pop marking */
bool_t
prov_dfs_dst_pop_marking
(dfs_stack_item_t * item) {
  encoding_t m = encoding_get(item->ref);
  attr_val_t color = encoding_attr_get(ATTR_PROV_COLOR, m);
  switch(color) {
  case PROV_RED: /* marking should not be red */
    assert(0);
    break;
  case PROV_GREEN: /* a green marking is unchanged */
    break;
  case PROV_ORANGE: /* an orange marking becomes red */
    encoding_attr_set(ATTR_PROV_COLOR, m, PROV_GREEN);
    break;
  case PROV_PURPLE:
    if(encoding_attr_get(ATTR_PROV_REVISIT, m)) {
      prov_dfs_fully_expand_top();
      return TRUE;
    }
    encoding_attr_set(ATTR_PROV_COLOR, m, PROV_RED);
  }
  return FALSE;
}

/* pop marking post */
void
prov_dfs_dst_pop_marking_post
(dfs_stack_item_t * item) {
  /* if the marking popped is RED and the current one is ORANGE, then
     the current one becomes PURPLE */
  dfs_stack_item_t top_item;
  encoding_t mp = encoding_get(item->ref);
  const int cp = encoding_attr_get(ATTR_PROV_COLOR, mp);
  darray_top(DFS.stack, &top_item);
  encoding_t m = encoding_get(top_item.ref);
  assert(m != mp && PROV_PURPLE != cp);
  const int c = encoding_attr_get(ATTR_PROV_COLOR, m);
  if(PROV_ORANGE == c && PROV_RED == cp) {
    encoding_attr_set(ATTR_PROV_COLOR, m, PROV_PURPLE);
  }
}

/* backedge */
bool_t
prov_dfs_dst_back_edge
(dfs_stack_item_t * item) {
  /* if neither the old (reached) marking neither the current one is
     GREEN then mark the old one as to revisit and color the current
     one in PURPLE */
  encoding_t cur = encoding_get(item->ref);
  encoding_t old = encoding_get(DFS.mdata.ref);
  if(PROV_GREEN != encoding_attr_get(ATTR_PROV_COLOR, cur) &&
     PROV_GREEN != encoding_attr_get(ATTR_PROV_COLOR, old)) {
    encoding_attr_set(ATTR_PROV_COLOR, cur, PROV_PURPLE);
    encoding_attr_set(ATTR_PROV_REVISIT, old, TRUE);
  }
  return FALSE;
}



/*****************************************************************************
 *  SRC PROVISO IN DFS
 *****************************************************************************/

/* pop marking */
bool_t
prov_dfs_src_pop_marking
(dfs_stack_item_t * item) {
  encoding_t m = encoding_get(item->ref);
  attr_val_t color = encoding_attr_get(ATTR_PROV_COLOR, m);
  switch(color) {
    case PROV_RED: /* marking should not be red */
      assert(0);
      break;
  case PROV_GREEN:
    assert(DFS.fexpanded);
    DFS.fexpanded --;
    break;
  case PROV_ORANGE: /* an orange marking becomes green when popped */
    encoding_attr_set(ATTR_PROV_COLOR, m, PROV_GREEN);
    break;
  case PROV_PURPLE: /* a purple marking becomes red when popped */
    encoding_attr_set(ATTR_PROV_COLOR, m, PROV_RED);
    break;
  }
  return FALSE;
}

/* backedge */
bool_t
prov_dfs_src_back_edge
(dfs_stack_item_t * item) {
  encoding_t cur = encoding_get(item->ref);
  if(PROV_GREEN != encoding_attr_get(ATTR_PROV_COLOR, cur)) {
    encoding_t old = encoding_get(DFS.mdata.ref);
    attr_val_t color_old = encoding_attr_get(ATTR_PROV_COLOR, old);
    if(PROV_GREEN != color_old) {
      if(PROV_RED != color_old
         && (encoding_attr_get(ATTR_PROV_FEXPANDED, cur) !=
             encoding_attr_get(ATTR_PROV_FEXPANDED, old))) {
        encoding_attr_set(ATTR_PROV_COLOR, cur, PROV_PURPLE);
      } else {
        prov_dfs_fully_expand_top();
        DFS.fexpanded ++;
        return TRUE;
      }     
    }
  }
  return FALSE;
}



/*****************************************************************************
 *  WEAK CYCLE OPTIMAL PROVISO
 *****************************************************************************/

typedef struct {
  trid_t t;
  uint32_t n;
} _prov_edge_t;

typedef struct {
  bool_t del;
  darray_t in;
  darray_t out;
  hset_ref_t ref;
} _prov_node_t;

typedef struct {
  bool_t initialised;
  uint32_t next_id;
  darray_t nodes;
} prov_data_t;

prov_data_t PROV = {
  .initialised = 0
};


void
prov_wopt_marking
(hset_ref_t ref) {
  if(!PROV.initialised) {
    PROV = (prov_data_t) {
      .next_id = 0,
      .initialised = 1,
      .nodes = darray_new(NULL, sizeof(_prov_node_t))
    };
  }
  const encoding_t e = encoding_get(ref);
  encoding_attr_set(ATTR_ID, e, PROV.next_id);
  PROV.next_id ++;
  _prov_node_t new_node = {
    .ref = ref,
    .del = FALSE,
    .in = darray_new(NULL, sizeof(_prov_edge_t)),
    .out = darray_new(NULL, sizeof(_prov_edge_t))
  };
  darray_push(PROV.nodes, &new_node);
}


void
prov_wopt_edge
(hset_ref_t ref_from,
 trid_t t,
 hset_ref_t ref_to) {
  const uint32_t id_from = encoding_attr_get(ATTR_ID, encoding_get(ref_from));
  const uint32_t id_to = encoding_attr_get(ATTR_ID, encoding_get(ref_to));
  const _prov_node_t * node_from = darray_get_ptr(PROV.nodes, id_from);
  const _prov_node_t * node_to = darray_get_ptr(PROV.nodes, id_to);
  _prov_edge_t edge_from = { .t = t, .n = id_to };
  _prov_edge_t edge_to = { .t = t, .n = id_from };
  darray_push(node_from->out, &edge_from);
  darray_push(node_to->in, &edge_to);
}


bool_t
_prov_del
(uint32_t id) {
  _prov_node_t * node = darray_get_ptr(PROV.nodes, id);
  if(node->del) {
    return FALSE;
  }
  const darray_t arr[] = {node->in, node->out};
  bool_t to_del = TRUE;
  if(!encoding_attr_get(ATTR_PROV_SAFE, encoding_get(node->ref))) {
    to_del = FALSE;
    for(int iarr = 0; !to_del && iarr < 2; iarr ++) {
      to_del = TRUE;
      for(int i = 0; to_del && i < darray_size(arr[iarr]); i ++) {
        const _prov_edge_t * edge = darray_get_ptr(arr[iarr], i);
        const _prov_node_t * end = darray_get_ptr(PROV.nodes, edge->n);
        if(!end->del) {
          to_del = FALSE;
        }
      }
    }
  }

  if(to_del) {
    node->del = TRUE;
  }
  return to_del;
}


void
_prov_del_all
() {
  bool_t deleted = TRUE;
  while(deleted) {
    deleted = FALSE;
    for(uint32_t id = 0; id < darray_size(PROV.nodes); id ++) {
      deleted = _prov_del(id) || deleted;
    }
  }
}


bool_t
prov_wopt
(hset_ref_t * ref) {
  _prov_del_all();
  for(uint32_t id = 0; id < darray_size(PROV.nodes); id ++) {
    const _prov_node_t * node = darray_get_ptr(PROV.nodes, id);
    if(!node->del) {
      *ref = node->ref;
      return TRUE;
    }
  }

  /* final check - undelete all nodes then retry to delete every node.
     finally check that all nodes have been deleted. */
  for(uint32_t id = 0; id < darray_size(PROV.nodes); id ++) {
    ((_prov_node_t *) darray_get_ptr(PROV.nodes, id))->del = FALSE;
  }
  _prov_del_all();
  for(uint32_t id = 0; id < darray_size(PROV.nodes); id ++) {
    assert(((_prov_node_t *) darray_get_ptr(PROV.nodes, id))->del);
  }
  return FALSE;
}
