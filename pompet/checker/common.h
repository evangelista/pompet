#ifndef LIB_COMMON
#define LIB_COMMON

#include "includes.h"

typedef uint64_t hkey_t;

typedef uint32_t prog_t;

typedef uint8_t bool_t;
#define FALSE 0
#define TRUE  1

typedef enum {
  LESS,
  EQUAL,
  GREATER
} order_t;

typedef struct {
  struct timeval start;
  uint64_t value;
  char started;
} nsec_timer_t;


hkey_t
string_hash
(char * v,
 unsigned int len);


hkey_t
string_hash_init
(char * v,
 unsigned int len,
 hkey_t init);


void
nsec_timer_init
(nsec_timer_t * t);


void
nsec_timer_start
(nsec_timer_t * t);


void
nsec_timer_stop
(nsec_timer_t * t);

#endif
