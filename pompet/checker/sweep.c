#include "sweep.h"
#include "ctx.h"
#include "ptnet.h"
#include "encoding.h"
#include "attrs.h"
#include "config.h"
#include "prov.h"

#define PQUEUE_NO_BLOCKS 1048576
#define PQUEUE_BLOCKS_LOOP(_i) for(int _i = 0; _i < PQUEUE_NO_BLOCKS; _i ++)

typedef struct {
  prog_t prog;
  hset_ref_t ref;
} pqueue_item_t;

typedef struct {
  uint32_t next;
  uint32_t size;
  darray_t blocks[PQUEUE_NO_BLOCKS];
} struct_pqueue_t;

typedef struct_pqueue_t * pqueue_t;


prog_t
prog_measure
(marking_t m);


pqueue_t
pqueue_new
() {
  pqueue_t result = malloc(sizeof(struct_pqueue_t));
  result->next = 0;
  result->size = 0;
  PQUEUE_BLOCKS_LOOP(i) {
    result->blocks[i] = darray_new(NULL, sizeof(hset_ref_t));
  }
  return result;
}


void
pqueue_free
(pqueue_t pq) {
  PQUEUE_BLOCKS_LOOP(i) {
    darray_free(pq->blocks[i]);
  }
  free(pq);
}


void
pqueue_dequeue
(pqueue_t pq,
 pqueue_item_t * item) {
  bool_t loop = 1;
  pq->size --;
  while(loop) {
    if(darray_size(pq->blocks[pq->next]) > 0) {
      item->prog = pq->next;
      darray_pop(pq->blocks[pq->next], &(item->ref));
      loop = 0;
    } else {
      pq->next = (pq->next + 1) % PQUEUE_NO_BLOCKS;
    }
  }
}


void
pqueue_enqueue
(pqueue_t pq,
 pqueue_item_t * item) {
  assert(item->prog < PQUEUE_NO_BLOCKS);
  if(item->prog < pq->next) {
    pq->next = item->prog;
  }
  pq->size ++;
  darray_push(pq->blocks[item->prog], &(item->ref));
}


uint32_t
pqueue_size
(pqueue_t pq) {
  return pq->size;
}


void
sweep
() {
  heap_t heap;
  marking_t now;
  hset_mdata_t mdata;
  pqueue_t pq[2];
  uint8_t step_now = 0, step_next = 1;
  pqueue_item_t item;
  darray_t garbage = darray_new(NULL, sizeof(hset_ref_t));
  prog_t prev;

  for(int i = 0; i < 2; i ++) {
    pq[i] = pqueue_new();
  }
  heap = local_heap_new();
  now = marking_copy(NET.m0, heap);
  ctx_marking_insert(now, &mdata);
  item.prog = prog_measure(now);
  item.ref = mdata.ref;
  pqueue_enqueue(pq[step_next], &item);

  while(pqueue_size(pq[step_next]) > 0) {

    step_now ^= 1;
    step_next ^= 1;
    prev = 0;

    assert((step_now != step_next) && (step_now + step_next == 1));
    assert(pqueue_size(pq[step_next]) == 0);
    assert(pqueue_size(pq[step_now]) > 0);

    /* a sweep */
    while(pqueue_size(pq[step_now]) > 0) {
      heap_reset(heap);

      /* dequeue the next marking and decode it */
      pqueue_dequeue(pq[step_now], &item);
      const encoding_t e = encoding_get(item.ref);
      now = marking_unserialise(encoding_pos_marking(e), heap);

      /*  garbage collection */
      if(prev < item.prog) {
        while(darray_size(garbage) > 0) {
          hset_ref_t gref;
          darray_pop(garbage, &gref);
          ctx_marking_delete(gref);
        }
      }
      prev = item.prog;

      /* compute its set of enabled transitions and reduce it */
      list_t en = marking_enabled(now, heap);

      /* process enabled transitions */
      list_iter_t it;
      list_for_each(en, it) {
        const trid_t t = * ((trid_t *) list_iter_item(it));
        trans_exec(t, now);
        ctx_trans_executed();
        if(ctx_marking_insert(now, &mdata)) {
          pqueue_item_t item_next;
          item_next.prog = prog_measure(now);
          item_next.ref = mdata.ref;
          if(item_next.prog >= item.prog) {
            pqueue_enqueue(pq[step_now], &item_next);
          } else {
            pqueue_enqueue(pq[step_next], &item_next);
          }
        }
        trans_undo(t, now);
      }
      darray_push(garbage, &item.ref);
      CTX.states_processed ++;
    }
  }

  heap_free(heap);
  for(int i = 0; i < 2; i ++) {
    pqueue_free(pq[i]);
  }
  darray_free(garbage);
}
