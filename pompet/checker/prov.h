#ifndef LIB_PROV
#define LIB_PROV

#include "includes.h"
#include "dfs.h"
#include "hset.h"

bool_t
prov_dfs_dst_pop_marking
(dfs_stack_item_t * item);

void
prov_dfs_dst_pop_marking_post
(dfs_stack_item_t * item);

bool_t
prov_dfs_dst_back_edge
(dfs_stack_item_t * item);

bool_t
prov_dfs_src_pop_marking
(dfs_stack_item_t * item);

bool_t
prov_dfs_src_back_edge
(dfs_stack_item_t * item);

void
prov_wopt_marking
(hset_ref_t ref);

void
prov_wopt_edge
(hset_ref_t ref_from,
 trid_t t,
 hset_ref_t ref_to);

bool_t
prov_wopt
(hset_ref_t * ref);

#endif
