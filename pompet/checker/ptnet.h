#ifndef LIB_PTNET
#define LIB_PTNET

#include "includes.h"
#include "common.h"
#include "heap.h"
#include "list.h"
#include "bit_stream.h"
#include "darray.h"

typedef uint32_t token_t;

typedef uint32_t plid_t;

typedef uint32_t trid_t;

#define TRID_NULL UINT32_MAX

typedef uint32_t unid_t;

typedef struct {
  plid_t pid;
  token_t tok;
} marked_place_t;

typedef struct {
  uint32_t id;
  int32_t tok;
} arc_t;

typedef struct {
  list_t marked;
  heap_t heap;
} struct_marking_t;

typedef struct_marking_t * marking_t;

typedef trid_t trans_t;

typedef uint32_t trans_id_t;

typedef enum { 
  STUB_ALGO_NONE,
  STUB_ALGO_CLODEL,
  STUB_ALGO_CLOSURE,
  STUB_ALGO_CLOSURE_OPT,
  STUB_ALGO_DELETION
} stub_algo_t;

typedef enum { 
  STUB_TRANS_CHOICE_FST,
  STUB_TRANS_CHOICE_MAX,
  STUB_TRANS_CHOICE_MAXFWD,
  STUB_TRANS_CHOICE_MIN,
  STUB_TRANS_CHOICE_MINFWD,
  STUB_TRANS_CHOICE_RND
} stub_trans_choice_t;

typedef enum {
  STUB_SCAPEGOAT_CHOICE_FST,
  STUB_SCAPEGOAT_CHOICE_MAX,
  STUB_SCAPEGOAT_CHOICE_MAXEN,
  STUB_SCAPEGOAT_CHOICE_MAXFWD,
  STUB_SCAPEGOAT_CHOICE_MIN,
  STUB_SCAPEGOAT_CHOICE_MINEN,
  STUB_SCAPEGOAT_CHOICE_MINFWD,
  STUB_SCAPEGOAT_CHOICE_RND
} stub_scapegoat_choice_t;

typedef enum {
  AND_NODE,
  OR_NODE
} del_graph_node_type_t;

typedef enum {
  PLACE_NODE,
  TRANS_NODE
} del_graph_content_type_t;

typedef struct  {
  uint32_t nid;
  del_graph_node_type_t ntype;
  del_graph_content_type_t ctype;
  list_t pred;
} del_graph_node_t;

typedef struct {
  bool_t * deleted;
  bool_t * protected;
  uint16_t * ref;
  del_graph_node_t * nodes;
  uint32_t nodes_nb;
} del_graph_t;

typedef struct {
  char *      inc_pre_str;
  char *      inc_post_str;
  char *      m0_str;

  uint32_t *  inc_pre;
  uint32_t *  inc_post;

  uint32_t    pl_nb;
  uint32_t    tr_nb;
  uint32_t    un_nb;  
  uint32_t    arcs_nb;

  uint32_t    pl_nb_width;
  uint32_t    pl_id_width;

  char *      name;
  char **     tr_names;
  char **     pl_names;

  marking_t   m0;
  
  uint32_t *  post_p_nb;
  arc_t **    post_p;

  uint32_t *  pre_t_nb;
  arc_t **    pre_t;
  
  uint32_t *  up_t_nb;
  arc_t **    up_t;

  uint32_t *  conf_t_nb;
  trid_t **   conf_t;
  
  uint32_t *  add_p_nb;
  trid_t **   add_p;

  uint32_t *  rem_p_nb;
  trid_t **   rem_p;

  uint32_t *  pl_u_nb;
  plid_t **   pl_u;
  
  unid_t *    unit_p;

  del_graph_t del_graph;
  bool_t      del_graph_init;

  bool_t **   DF;  /* dynamic dependency relation from the full graph */
  bool_t **   DS;  /* static dependency relation from net structure */
} ptnet_t;

extern ptnet_t NET;

typedef struct {
  list_t included;
  bool_t (* is_new_func) (marking_t);
} stub_data_t;


void
init_ptnet
();


/* marking related functions */
marking_t
marking_initial
(heap_t heap);

marking_t
marking_copy
(marking_t s,
 heap_t heap);

void
marking_free
(marking_t s);

list_t
marking_enabled
(marking_t s,
 heap_t heap);

void
marking_print
(marking_t s,
 FILE * out);

unsigned int
marking_char_size
(marking_t s);

void
marking_serialise
(marking_t s,
 char * v,
 uint16_t * size);

marking_t
marking_unserialise
(char * v,
 heap_t heap);

void
marking_stub
(marking_t s,
 list_t en,
 stub_data_t * data);


/* transition related functions */
void
trans_exec
(trans_t e,
 marking_t s);

void
trans_undo
(trans_t e,
 marking_t s);

order_t
trans_cmp
(trans_t e,
 trans_t f);

void
trans_print
(trans_t e,
 FILE * out);


/* net related functions */
void
ptnet_load_post
();

#endif
