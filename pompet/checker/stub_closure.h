#ifndef LIB_STUB_CLOSURE
#define LIB_STUB_CLOSURE

#include "includes.h"
#include "common.h"
#include "list.h"
#include "ptnet.h"


void
stub_closure
(list_t en,
 void * data,
 token_t * m,
 bool_t * is_en,
 bool_t * is_fwd,
 bool_t * is_stub);

#endif
