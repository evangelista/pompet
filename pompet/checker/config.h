#ifndef LIB_CONFIG
#define LIB_CONFIG

#include "includes.h"
#include "common.h"
#include "ptnet.h"

typedef enum {
  ALGO_BFS,
  ALGO_DFS,
  ALGO_SWEEP
} algo_t;

typedef enum {
  PROV_NONE,
  PROV_SRC,
  PROV_DST,
  PROV_WOPT
} prov_t;

typedef struct {
  algo_t algo;
  stub_algo_t stub_algo;
  stub_trans_choice_t stub_trans_choice;
  stub_scapegoat_choice_t stub_scapegoat_choice;
  uint32_t init_hash_size;
  uint32_t print_ctr;
  prov_t prov;
  char edge_lean;
  char store_hash_keys;
  char graph_analysis;
  char use_units;
} config_t;

extern config_t CFG;

#endif
