#ifndef LIB_PTNET_LOADER
#define LIB_PTNET_LOADER

#include "includes.h"
#include "common.h"
#include "heap.h"
#include "list.h"
#include "bit_stream.h"
#include "ptnet.h"


void
ptnet_load
();

#endif
