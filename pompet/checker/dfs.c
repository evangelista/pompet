#include "dfs.h"
#include "prov.h"
#include "ctx.h"
#include "encoding.h"

dfs_data_t DFS;


bool_t
_dfs_next_trans
(trid_t * t) {
  dfs_stack_item_t * item = darray_top_ptr(DFS.stack);
  if(item->cur >= item->no_en) {
    return FALSE;
  }
  *t = item->en[item->cur ++];
  return TRUE;
}


dfs_pop_result_t
_dfs_pop
(trid_t * t) {
  dfs_stack_item_t * item = darray_top_ptr(DFS.stack);
  encoding_t m = encoding_get(item->ref);

  /* proviso handler for a popped marking */
  if(DFS.prov_pop && DFS.prov_pop(item)) {
    return DFS_POP_REVISIT;
  }

  darray_pop(DFS.stack, NULL);

  /* reset open attribute */
  if(ATTRS.attrs[ATTR_OPEN]) {
    assert(encoding_attr_get(ATTR_OPEN, m));
    encoding_attr_set(ATTR_OPEN, m, 1);
  }

  if(0 == darray_size(DFS.stack)) {
    return DFS_POP_EMPTY;
  }

  heap_set_position(DFS.stack_heap, item->heap_pos);
  item = darray_top_ptr(DFS.stack);
  *t = item->en[item->cur - 1];
  return DFS_POP_OK;
}


void
_dfs_new_marking
(trid_t t_prev) {
  encoding_t m = encoding_get(DFS.mdata.ref);

  /* set some marking attributes */
  if(ATTRS.attrs[ATTR_OPEN]) {
    encoding_attr_set(ATTR_OPEN, m, 1);
  }
  if(ATTRS.attrs[ATTR_PROV_FEXPANDED]) {
    encoding_attr_set(ATTR_PROV_FEXPANDED, m, DFS.fexpanded);
  }

  /* compute the set of enabled transitions and reduce it */
  list_t en = marking_enabled(DFS.now, DFS.heap);
  const int no_en_before_reduction = list_size(en);
  if(0 == no_en_before_reduction) {
    CTX.states_dead ++;
  }
  if(CFG.stub_algo != STUB_ALGO_NONE) {
    marking_stub(DFS.now, en, &DFS.stub_data);
  }
  if(CFG.edge_lean && t_prev != TRID_NULL) {
    edge_lean(t_prev, en);
  }

  /* push the marking */
  const uint16_t no_en = list_size(en);
  dfs_stack_item_t item = {
    .ref = DFS.mdata.ref,
    .heap_pos = heap_get_position(DFS.stack_heap),
    .no_en = no_en,
    .cur = 0,
    .en = mem_alloc(DFS.stack_heap, sizeof(trid_t) * no_en)
  };
  int i = 0;
  list_iter_t it;
  list_for_each(en, it) {
    item.en[i ++] = * ((trid_t *) list_iter_item(it));
  }
  darray_push(DFS.stack, &item);

  /* a fully expanded state is green */
  if(no_en_before_reduction == no_en) {
    if(ATTRS.attrs[ATTR_PROV_COLOR]) {
      DFS.fexpanded ++;
      encoding_attr_set(ATTR_PROV_COLOR, m, PROV_GREEN);
    }
  }
}


void
dfs
() {
  bool_t loop = TRUE;

  /* reset all bytes of DFS to O */
  memset(&DFS, 0, sizeof(dfs_data_t));
  DFS.stub_data.is_new_func = ctx_marking_is_new;

  /* initialise proviso handler functions */
  if(PROV_DST == CFG.prov) {
    DFS.prov_pop = &prov_dfs_dst_pop_marking;
    DFS.prov_pop_post = &prov_dfs_dst_pop_marking_post;
    DFS.prov_back = &prov_dfs_dst_back_edge;
  } else if(PROV_SRC == CFG.prov) {
    DFS.prov_pop = &prov_dfs_src_pop_marking;
    DFS.prov_back = &prov_dfs_src_back_edge;
  }

  /* initialise some heaps */
  DFS.heap = local_heap_new();
  DFS.stack_heap = local_heap_new();

  /* insert the initial marking in the hash table in the stack */
  DFS.now = marking_copy(NET.m0, DFS.heap);
  DFS.stack = darray_new(NULL, sizeof(dfs_stack_item_t));
  ctx_marking_insert(DFS.now, &DFS.mdata);
  _dfs_new_marking(TRID_NULL);

  /* main loop */
  while(loop) {
    trid_t t;
    dfs_stack_item_t top_item;

    darray_top(DFS.stack, &top_item);

    /* put now in a new heap if the current one exceeds some bound */
    if(heap_size(DFS.heap) >= 100000) {
      marking_t copy = marking_copy(DFS.now, NULL);
      heap_reset(DFS.heap);
      DFS.now = marking_copy(copy, DFS.heap);
      marking_free(copy);
    }

    /* still one transition t to fire for marking now */
    if(_dfs_next_trans(&t)) {

      trans_exec(t, DFS.now);
      ctx_trans_executed();

      /* transition leads to a new marking */
      if(ctx_marking_insert(DFS.now, &DFS.mdata)) {
        _dfs_new_marking(t);
      }
      
      /* transition leads to an old marking */
      else {
        trans_undo(t, DFS.now);

        /* proviso handler for a back edge */
        if(DFS.prov_back) {
          DFS.prov_back(&top_item);
        }
      }
    }

    /* no more transition to fire for marking now */
    else {
      dfs_pop_result_t pop_result = _dfs_pop(&t);
      if(DFS_POP_EMPTY == pop_result) {
        loop = FALSE;
      } else if(DFS_POP_OK == pop_result) {
        trans_undo(t, DFS.now);

        /* proviso handler for a popped marking (post popping) */
        if(DFS.prov_pop_post) {
          DFS.prov_pop_post(&top_item);
        }
      }
      CTX.states_processed ++;
    }
  }

  /* free everything */
  heap_free(DFS.heap);
  heap_free(DFS.stack_heap);
  darray_free(DFS.stack);
}
