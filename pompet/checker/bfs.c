#include "bfs.h"
#include "ctx.h"
#include "ptnet.h"
#include "encoding.h"
#include "attrs.h"
#include "config.h"
#include "prov.h"
#include "graph.h"

bfs_data_t BFS;

bool_t _bfs_queue_next
(bfs_queue_item_t ** item) {
  if(list_is_empty(BFS.queue)) {
    return FALSE;
  }
  bfs_queue_slot_t * slot = (bfs_queue_slot_t *) list_first(BFS.queue);
  *item = &(slot->items[slot->next]);
  return TRUE;
}


void _bfs_queue_dequeue
() {
  bfs_queue_slot_t * slot = (bfs_queue_slot_t *) list_first(BFS.queue);
  slot->next ++;
  if(slot->next == slot->no_items) {
    list_pick_first(BFS.queue, NULL);
  }
}


void _bfs_queue_put
(bfs_queue_item_t item) {
  bool_t new_slot;
  bfs_queue_slot_t * last_slot = NULL;
  if(list_is_empty(BFS.queue)) {
    new_slot = 1;
  } else {
    last_slot = (bfs_queue_slot_t *) list_last(BFS.queue);
    new_slot = last_slot->no_items == BFS_QUEUE_SLOT_SIZE;
  }
  if(new_slot) {
    bfs_queue_slot_t new_slot;
    new_slot.no_items = 1;
    new_slot.next = 0;
    new_slot.items[0] = item;
    list_append(BFS.queue, &new_slot);
  } else {
    last_slot->items[last_slot->no_items ++] = item;
  }
}


void
bfs
() {
  bfs_queue_item_t * item;
  hset_mdata_t mdata;
  heap_t heap;
  marking_t now;
  stub_data_t stub_data = { .is_new_func = ctx_marking_is_new };
  uint32_t cur = 1, nxt = 0;

  memset(&BFS, 0, sizeof(bfs_data_t));
  BFS.queue = list_new(NULL, sizeof(bfs_queue_slot_t));
  heap = local_heap_new();
  now = marking_copy(NET.m0, heap);
  ctx_marking_insert(now, &mdata);
  _bfs_queue_put((bfs_queue_item_t) { .ref = mdata.ref, .fexpand = FALSE });
  
  /* new marking for the weak cycle proviso optimal or graph
     analysis */
  if(CFG.prov == PROV_WOPT) {
    prov_wopt_marking(mdata.ref);
  }
  if(CFG.graph_analysis) {
    graph_new_marking(mdata.ref, now);
  }

 bfs_loop:
  while(_bfs_queue_next(&item)) {

    bool_t dequeue = TRUE;

    /* get the next marking */
    heap_reset(heap);
    const encoding_t e = encoding_get(item->ref);
    now = marking_unserialise(encoding_pos_marking(e), heap);

    /* compute its set of enabled transitions and reduce it */
    list_t en = marking_enabled(now, heap);
    const int no_en_before_reduction = list_size(en);
    if(0 == no_en_before_reduction) {
      CTX.states_dead ++;
    }
    if(!item->fexpand && CFG.stub_algo != STUB_ALGO_NONE) {
      marking_stub(now, en, &stub_data);
    }
    const int no_en = list_size(en);
    bool_t now_safe = no_en_before_reduction == no_en;
    if(now_safe && ATTRS.attrs[ATTR_PROV_SAFE]) {
      encoding_attr_set(ATTR_PROV_SAFE, e, 1);
    }

    /* process enabled transitions */
    list_iter_t it;
    list_for_each(en, it) {
      const trid_t t = * ((trid_t *) list_iter_item(it));
      trans_exec(t, now);
      ctx_trans_executed();
      if(ctx_marking_insert(now, &mdata)) {
        nxt ++;
        _bfs_queue_put(
          (bfs_queue_item_t) { .ref = mdata.ref, .fexpand = FALSE }
        );

        /* new marking for the weak cycle proviso optimal or graph
           analysis */
        if(CFG.prov == PROV_WOPT) {
          prov_wopt_marking(mdata.ref);
        }
        if(CFG.graph_analysis) {
          graph_new_marking(mdata.ref, now);
        }
        
      } else if(!now_safe && (CFG.prov == PROV_SRC || CFG.prov == PROV_DST)) {

        /* check proviso */
        const encoding_t esucc = encoding_get(mdata.ref);
        const bool_t succ_safe = encoding_attr_get(ATTR_PROV_SAFE, esucc);
        if(!succ_safe) {
          if(CFG.prov == PROV_SRC) {
            dequeue = FALSE;
            item->fexpand = TRUE;
            CTX.arcs -= no_en;
            trans_undo(t, now);
            break;
          } else if(CFG.prov == PROV_DST) {
            encoding_attr_set(ATTR_PROV_SAFE, esucc, 1);
            _bfs_queue_put(
              (bfs_queue_item_t) { .ref = mdata.ref, .fexpand = TRUE }
            );
          }
        }

      }

      /* new edge for the weak cycle proviso optimal or graph
         analysis */
      if(CFG.prov == PROV_WOPT) {
        prov_wopt_edge(item->ref, t, mdata.ref);
      }
      if(CFG.graph_analysis) {
        graph_new_edge(item->ref, t, mdata.ref);
      }

      trans_undo(t, now);
    }

    if(dequeue) {
      _bfs_queue_dequeue();
      CTX.states_processed ++;
      cur --;
      if(cur == 0) {
        cur = nxt;
        nxt = 0;
        CTX.levels ++;
      }
    }
  }

  /* check the weak cycle proviso optimal */
  if(CFG.prov == PROV_WOPT) {
    hset_ref_t to_fully_expand;
    if(prov_wopt(&to_fully_expand)) {
      const bfs_queue_item_t it = { .ref = to_fully_expand, .fexpand = TRUE };
      _bfs_queue_put(it);
      goto bfs_loop;
    }
  }
  
  if(CFG.graph_analysis) {
    graph_report();
  }

  heap_free(heap);
}
