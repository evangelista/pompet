#include "includes.h"
#include "ptnet_loader.h"
#include "bfs.h"
#include "dfs.h"
#include "sweep.h"
#include "heap.h"
#include "bit_stream.h"
#include "config.h"
#include "attrs.h"
#include "ctx.h"


void
print_help
(char * cmd) {
  printf("usage: %s [OPTIONS]\n", cmd);
  printf("options:\n");
  printf("   -abfs\n");
  printf("   -adfs\n");
  printf("   -asweep\n");
  printf("   -g\n");
  printf("   -hN\n");
  printf("   -nN\n");
  printf("   -pdel\n");
  printf("   -pclo\n");
  printf("   -pcloopt\n");
  printf("   -pclodel\n");
  printf("   -q\n");
  printf("   -rsrc\n");
  printf("   -rdst\n");
  printf("   -rwopt\n");
  printf("   -sfst\n");
  printf("   -srnd\n");
  printf("   -smin\n");
  printf("   -sminen\n");
  printf("   -sminfwd\n");
  printf("   -smax\n");
  printf("   -smaxen\n");
  printf("   -smaxfwd\n");
  printf("   -tmin\n");
  printf("   -tfst\n");
  printf("   -trnd\n");
  printf("   -tmin\n");
  printf("   -tminfwd\n");
  printf("   -tmax\n");
  printf("   -tmaxfwd\n");
  printf("   -ofile\n");
  printf("   -u\n");
  printf("   -h\n");
}


void
on_sigint
(int signal) {
  ctx_print_statistics();
  exit(1);
}


int
main
(int argc,
 char ** argv) {
  /* parse arguments */
  for(int i = 1; i < argc; i ++) {
    char * a = argv[i];
    if(!strcmp(a, "-abfs")) { /* BFS */
      CFG.algo = ALGO_BFS;
    } else if(!strcmp(a, "-adfs")) { /* DFS */
      CFG.algo = ALGO_DFS;
    } else if(!strcmp(a, "-asweep")) { /* Sweep-Line */
      CFG.algo = ALGO_SWEEP;
    } else if(!strcmp(a, "-e")) { /* edge lean algorithm */
      CFG.edge_lean = TRUE;
    } else if(!strcmp(a, "-h")) { /* help */
      print_help(argv[0]);
      exit(0);
    } else if(!strncmp(a, "-h", 2)) {
      int n;
      sscanf(a, "-h%d", &n);
      if(0 != n) {
        CFG.init_hash_size = n;
      }
    } else if(!strncmp(a, "-n", 2)) {
      int n;
      sscanf(a, "-n%d", &n);
      CFG.print_ctr = n;
    } else if(!strcmp(a, "-g")) { /* reachability graph analysis */
      CFG.graph_analysis = TRUE;
    } else if(!strcmp(a, "-pdel")) { /* stubborn set algorithm */
      CFG.stub_algo = STUB_ALGO_DELETION;
    } else if(!strcmp(a, "-pclo")) {
      CFG.stub_algo = STUB_ALGO_CLOSURE;
    } else if(!strcmp(a, "-pclodel")) {
      CFG.stub_algo = STUB_ALGO_CLODEL;
    } else if(!strcmp(a, "-pcloopt")) {
      CFG.stub_algo = STUB_ALGO_CLOSURE_OPT;
    } else if(!strcmp(a, "-tfst")) { /* transition choice strategy */
      CFG.stub_trans_choice = STUB_TRANS_CHOICE_FST;
    } else if(!strcmp(a, "-trnd")) {
      CFG.stub_trans_choice = STUB_TRANS_CHOICE_RND;
    } else if(!strcmp(a, "-tmin")) {
      CFG.stub_trans_choice = STUB_TRANS_CHOICE_MIN;
    } else if(!strcmp(a, "-tminfwd")) {
      CFG.stub_trans_choice = STUB_TRANS_CHOICE_MINFWD;
    } else if(!strcmp(a, "-tmax")) {
      CFG.stub_trans_choice = STUB_TRANS_CHOICE_MAX;
    } else if(!strcmp(a, "-tmaxfwd")) {
      CFG.stub_trans_choice = STUB_TRANS_CHOICE_MAXFWD;
    } else if(!strcmp(a, "-sfst")) { /* scapegoat choice strategy */
      CFG.stub_scapegoat_choice = STUB_SCAPEGOAT_CHOICE_FST;
    } else if(!strcmp(a, "-srnd")) {
      CFG.stub_scapegoat_choice = STUB_SCAPEGOAT_CHOICE_RND;
    } else if(!strcmp(a, "-smin")) {
      CFG.stub_scapegoat_choice = STUB_SCAPEGOAT_CHOICE_MIN;
    } else if(!strcmp(a, "-sminen")) {
      CFG.stub_scapegoat_choice = STUB_SCAPEGOAT_CHOICE_MINEN;
    } else if(!strcmp(a, "-sminfwd")) {
      CFG.stub_scapegoat_choice = STUB_SCAPEGOAT_CHOICE_MINFWD;
    } else if(!strcmp(a, "-smax")) {
      CFG.stub_scapegoat_choice = STUB_SCAPEGOAT_CHOICE_MAX;
    } else if(!strcmp(a, "-smaxen")) {
      CFG.stub_scapegoat_choice = STUB_SCAPEGOAT_CHOICE_MAXEN;
    } else if(!strcmp(a, "-smaxfwd")) {
      CFG.stub_scapegoat_choice = STUB_SCAPEGOAT_CHOICE_MAXFWD;
    } else if(!strcmp(a, "-rsrc")) { /* proviso */
      CFG.prov = PROV_SRC;
    } else if(!strcmp(a, "-rdst")) {
      CFG.prov = PROV_DST;
    } else if(!strcmp(a, "-rwopt")) {
      CFG.prov = PROV_WOPT;
    } else if(!strncmp(a, "-o", 2) && strlen(a) > 2) {
      continue;  /* argument handled below */
    } else if(!strcmp(a, "-u")) {
      CFG.use_units = TRUE;
    } else {
      printf("warning: unrecognized option: %s\n", a);
    }
  }

  /* initialise everything */
  init_attrs();
  init_heap();
  init_bit_stream();
  init_ptnet();
  init_ctx();

  /* some arguments must be parsed after initialisation */
  for(int i = 1; i < argc; i ++) {
    char * a = argv[i];
    if(!strncmp(a, "-o", 2) && strlen(a) > 2) { /* output */
      const int lg = strlen(a);
      CTX.out = malloc(lg - 1);
      memcpy(CTX.out, a + 2, lg - 2);
      a[lg - 1] = 0;
    }
  }

  signal(SIGINT, on_sigint);

  switch(CFG.algo) {
  case ALGO_DFS:
    dfs();
    break;
  case ALGO_BFS:
    bfs();
    break;
  case ALGO_SWEEP:
    sweep();
    break;
  }
  
  CTX.search_terminated = 1;
  ctx_print_statistics();
  ctx_output_statistics();

  free_ctx();
  exit(0);
}
