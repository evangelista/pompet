#!/usr/bin/env python3

"""Definition of class Trans."""

import typing as tp

from .node import Node


class Trans(Node):
    """Class Trans implements net transitions."""

    def to_prod(self, pr: tp.Callable[[str], None]) -> None:
        pr(f'#trans T{self.num}\n')
        for arcs, atype in [
                (self.input_arcs, 'in '),
                (self.output_arcs, 'out')
        ]:
            pr(atype + ' {')
            for a in arcs:
                pid = a.get_other_end(self.nid)
                n = self.net.get_node(pid)
                pr(f' P{n.num}: ')
                if a.val > 1:
                    pr(str(a.val))
                pr('<..>;')
            pr('}\n')
        pr('#endtr\n')

    def to_pnml(self, pr: tp.Callable[[str], None]) -> None:
        out = f'<transition id="{self.nid}">'
        out += f'<name><text>{self.name}</text></name>'
        out += '</transition>\n'
        pr(out)

    def conflict(self) -> tp.List['Trans']:
        """Get the list of transitons in conflict with self.

        [V98] page 506, condition 2 of second itemize.

        """
        net = self.net
        pre = self.get_pre_set()
        return sorted([
            u
            for _, p in pre
            for u in net.T
            if u != self and (
                    min(net.get_weight(self, p), net.get_weight(u, p))
                    < min(net.get_weight(p, self), net.get_weight(p, u))
            )
        ], key = lambda u: u.num)
