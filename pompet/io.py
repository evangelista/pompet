#!/usr/bin/env python3

"""Provide simple io primitives."""

import sys
import typing as tp


verbose: bool = False


def msg(m: str, prefix: str = '# ') -> None:
    """Output the m string to stdout (in VERBOSE mode only)."""
    if verbose:
        print(prefix + m)


def err(
        msg: str,
        code: tp.Optional[int] = 1,
        fmt: str = '# ERROR(code={code}): {msg}'
) -> None:
    """Print the error m string to stdout and exit if code is not None."""
    print(fmt.format(code=code, msg=msg))
    if code is not None:
        sys.exit(code)
