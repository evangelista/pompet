#!/usr/bin/env python3

"""Definition of class PTnet."""

import os
import io
import sys
import math
import csv
import random
import typing as tp
from xml.dom.minidom import (
    Document as XMLDocument, Element as XMLElement, parse as xml_parse
)

from .place import Place
from .trans import Trans
from .node import Node
from .unit import Unit
from .arc import Arc


class PTnet:

    def __init__(self) -> None:
        self.name: str = ""
        self.N: tp.Dict[str, Node] = dict()
        self.P: tp.List[Place] = []
        self.T: tp.List[Trans] = []
        self.A: tp.List[Arc] = []
        self.U: tp.List[Unit] = []

    def add_place(self, p: Place) -> None:
        self.P.append(p)
        assert p.nid not in self.N
        self.N[p.nid] = p

    def add_trans(self, t: Trans) -> None:
        self.T.append(t)
        assert t.nid not in self.N
        self.N[t.nid] = t

    def add_unit(self, u: Unit) -> None:
        self.U.append(u)

    def add_arc(self, a: Arc) -> None:
        self.A.append(a)
        for nid in [ a.src, a.dst ]:
            n = self.get_node(nid)
            assert n is not None
            n.add_arc(a)

    def get_weight(self, from_node: Node, to_node: Node) -> int:
        for arc in from_node.output_arcs:
            assert arc.src == from_node.nid
            if arc.dst == to_node.nid:
                return arc.val
        return 0

    def get_node(self, nid: str) -> Node:
        return self.N[nid]

    def del_node(self, n: Node) -> None:

        # remove n
        del self.N[n.nid]
        if isinstance(n, Place):
            self.P.remove(n)
        elif isinstance(n, Trans):
            self.T.remove(n)

        # remove all arcs connected to n
        del_arcs = {a for a in self.A if n.nid in {a.src, a.dst}}
        for x in self.T + self.P:
            x.input_arcs = list(set(x.input_arcs) - del_arcs)
            x.output_arcs = list(set(x.output_arcs) - del_arcs)
        self.A = list(set(self.A) - del_arcs)

    def agglomerate(self, p: Place) -> None:
        pre = tp.cast(tp.List[Trans], [t[1] for t in p.get_pre_set()])
        post = tp.cast(tp.List[Trans], [t[1] for t in p.get_post_set()])

        # create a new transition for each (h, f) in pre * post
        trans_map: tp.Dict[Trans, tp.List[Trans]] = dict()
        for h in pre:
            for f in post:
                nid = h.nid + "." + f.nid
                name = h.name + "." + f.name
                new_trans = Trans(0, nid, name, self)
                trans_map.setdefault(h, list()).append(new_trans)
                trans_map.setdefault(f, list()).append(new_trans)
                self.add_trans(new_trans)

        # create new arcs
        for a in self.A:
            src = self.get_node(a.src)
            dst = self.get_node(a.dst)
            if src in pre + post:
                assert isinstance(src, Trans) and isinstance(dst, Place)
                for t in trans_map.get(src, list()):
                    new_arc = Arc("0", t.nid, dst.nid, a.val)
                    self.add_arc(new_arc)
            elif dst in pre + post:
                assert isinstance(src, Place) and isinstance(dst, Trans)
                for t in trans_map.get(dst, list()):
                    new_arc = Arc("0", src.nid, t.nid, a.val)
                    self.add_arc(new_arc)

        # delete p and transitions of pre and post
        self.del_node(p)
        for x in pre + post:
            self.del_node(x)

        # recompute numbers
        for i, p in enumerate(self.P):
            p.num = i
        for i, t in enumerate(self.T):
            t.num = i

    def agglomerate_all(self) -> int:
        result = 0
        reduced = True
        while reduced:
            reduced = False
            for p in self.P:       
                if p.is_post_agglomerable():
                    self.agglomerate(p)
                    reduced = True
                    result += 1
                    break
        return result

    def _pre_or_post(self, src: str, dst: str) -> int:
        result = 0
        for a in self.A:
            if a.src == src and a.dst == dst:
                result += a.val
        return result

    def pre(self, p: Place, t: Trans) -> int:
        return sum(a.val for a in p.output_arcs if a.dst == t.nid)

    def post(self, p: Place, t: Trans) -> int:
        return sum(a.val for a in p.input_arcs if a.src == t.nid)

    def reduce(self) -> None:
        print(self.agglomerate_all())

    def compile(
            self,
            out_dir: str,
            dep_rel_file: tp.Optional[str] = None
    ) -> tp.List[str]:
        """Compile all source files for the net in directory out_dir."""
        def bit_length(n: int) -> int:
            if 0 == n:
                return 1
            log2 = math.log(n, 2)
            if log2 > int(log2):
                return int(log2) + 1
            return int(log2)

        if not os.path.exists(out_dir):
            os.mkdir(out_dir)
        result = list()
        path = os.path.join(out_dir, "ptnet_loader.c")
        fd = open(path, "w", encoding="utf-8")

        def cw(row: str)-> None:
            fd.write(row)

        cw("#include \"ptnet.h\"\n\n")

        # static dependency relation
        cw("void ptnet_load_static_dependency_relation(ptnet_t * N) {\n")
        if False:
            # DO NOT GENERATE FOR NOW
            cw("  bool_t * c;\n")
            cw("  N->DS = malloc(sizeof(char *) * N->tr_nb);\n")
            cw("  for(int t = 0; t < N->tr_nb; t ++) {\n")
            cw("    N->DS[t] = malloc(sizeof(char) * N->tr_nb);\n")
            cw("    memset(N->DS[t], 0, sizeof(char) * N->tr_nb);\n")
            cw("  }\n")
            for t in self.T:
                cw(f"  c = N->DS[{t.num}];\n")
                assign = " = ".join(
                    list(set(f"c[{v.num}]" for v in t.conflict()))
                )
                if assign != "":
                    cw(f"  {assign} = 1;\n")
        cw("}\n")

        # full dependency relation (GHV09, p.44, D_f)
        cw("void ptnet_load_full_dependency_relation(ptnet_t * N) {\n")
        if dep_rel_file is None:
            cw("  N->DF = NULL;\n")
        else:
            cw("  N->DF = malloc(sizeof(char *) * N->tr_nb);\n")
            cw("  for(int t = 0; t < N->tr_nb; t ++) {\n")
            cw("    N->DF[t] = malloc(sizeof(char) * N->tr_nb);\n")
            cw("    memset(N->DF[t], 0, sizeof(char) * N->tr_nb);\n")
            cw("  }\n")
            with open(dep_rel_file, encoding="utf-8") as pfd:
                reader = csv.DictReader(
                    pfd, delimiter=";", fieldnames=["r", "t", "u"]
                )
                for row in reader:
                    ti = next(t.num for t in self.T if t.nid == row["t"])
                    ui = next(u.num for u in self.T if u.nid == row["u"])
                    cw(f"  N->DF[{ti}][{ui}] = 1;\n")
        cw("}\n")
        cw("""
void
ptnet_load
(ptnet_t * N) {
  N->pl_nb = """ + str(len(self.P)) + """;
  N->tr_nb = """ + str(len(self.T)) + """;
  N->arcs_nb = """ + str(len(self.A)) + """;
  N->un_nb = """ + str(len(self.U)) + """;
  N->pl_nb_width = """ + str(bit_length(len(self.P) + 1)) + """;
  N->pl_id_width = """ + str(bit_length(len(self.P))) + """;
  N->pl_names = malloc(N->pl_nb * sizeof(char *));
  N->tr_names = malloc(N->tr_nb * sizeof(char *));
  N->pl_u_nb = malloc(N->un_nb * sizeof(uint32_t));
  N->pl_u = malloc(N->un_nb * sizeof(plid_t *));
  N->unit_p = malloc(N->pl_nb * sizeof(unid_t *));
""")
        # put pre and post incidence matrices in strings that can be loaded
        # in the ptnet_load_post function
        inc_pre: tp.Dict[tp.Tuple[Place, Trans], int] = dict()
        inc_post: tp.Dict[tp.Tuple[Place, Trans], int] = dict()
        for a in self.A:
            src = self.get_node(a.src)
            dst = self.get_node(a.dst)
            if isinstance(src, Place):
                inc = inc_pre
                idx = (src, tp.cast(Trans, dst))
            else:
                inc = inc_post
                idx = (tp.cast(Place, dst), tp.cast(Trans, src))
            inc[idx] = inc.get(idx, 0) + a.val
        for inc, inc_name in [(inc_pre, "inc_pre"), (inc_post, "inc_post")]:
            x = list()
            for pnum, p in enumerate(self.P):
                for tnum, t in enumerate(self.T):
                    val = inc.get((p, t), 0)
                    if val > 0:
                        x.append(f"{pnum};{tnum};{val}")
            var = f"{inc_name}_str"
            cw(f"  const char * {var} = \"" + ";".join(x) + "\";\n")
            cw(f"  N->{var} = malloc(strlen({var}) + 1);\n")
            cw(f"  memcpy(N->{var}, {var}, strlen({var}) + 1);\n")

        # same for the initial marking
        m0_str = ";".join(str(p.init) for p in self.P)
        cw(f"  N->m0_str = \"{m0_str}\";\n")

        for n, p in enumerate(self.P):
            cw(f"  N->pl_names[{n}] = \"{p.name}\";\n")
            unit = next((u for u in self.U if p.nid in u.places), None)
            if unit is None:
                cw(f"  N->unit_p[{n}] = UINT_MAX;\n")
            else:
                cw(f"  N->unit_p[{n}] = {unit.num};\n")
        for n, t in enumerate(self.T):
            cw(f"  N->tr_names[{n}] = \"{t.name}\";\n")
        for i, u in enumerate(self.U):
            lg = len(u.places)
            cw(f"  N->pl_u_nb[{i}] = {lg};\n")
            cw(f"  N->pl_u[{i}] = mem_alloc(NULL, sizeof(plid_t) * {lg});\n")
            for j, pname in enumerate(u.places):
                pnum = self.get_node(pname).num
                cw(f"  N->pl_u[{i}][{j}] = {pnum};\n")

        cw("  ptnet_load_static_dependency_relation(N);\n")
        cw("  ptnet_load_full_dependency_relation(N);\n")
        cw("}\n")
        result.append("ptnet_loader.c")
        for f in [
                self.compile_prog_measure_random,
                self.compile_prog_measure_struct,
                self.compile_prog_measure_unit
        ]:
            f(cw)
        fd.close()

        return result

    def to_prod(self, prod_file: str) -> None:
        with open(prod_file, "w", encoding="utf-8") as fd:
            def pr(line: str) -> None:
                fd.write(line)
            for p in self.P:
                p.to_prod(pr)
            for t in self.T:
                t.to_prod(pr)

    def to_pnml(self, pnml_file: tp.Optional[str]=None) -> None:
        if pnml_file is None:
            fd = sys.stdout
        else:
            fd = open(pnml_file, "w", encoding="utf-8")
        fd.write("""<?xml version="1.0"?>
<pnml xmlns="http://www.pnml.org/version-2009/grammar/pnml">
  <net id="the_net" type="http://www.pnml.org/version-2009/grammar/ptnet">
    <page id="the_page">
""")
        def pr(line: str) -> None:
            fd.write("      " + line)
        for p in self.P:
            p.to_pnml(pr)
        for t in self.T:
            t.to_pnml(pr)
        for a in self.A:
            a.to_pnml(pr)
        fd.write("""    </page>
  </net>
</pnml>""")
        fd.close()

    @staticmethod
    def from_pnml(pnml: str, shuffle: bool = False) -> "PTnet":
        def get_attr(e: XMLElement, name: str) -> str:
            for i in range(0, e.attributes.length):
                if e.attributes.item(i).nodeName == name:
                    return tp.cast(str, e.attributes.item(i).value)
            raise ValueError(f"attribute {name} not found")

        def get_node_attr(e: XMLElement, attr: str, default: str = "") -> str:
            try:
                return tp.cast(
                    str,
                    e.getElementsByTagName(attr)[0] \
                    .getElementsByTagName("text")[0] \
                    .childNodes[0].nodeValue
                )
            except:
                return default

        def get_nodes(tag: str) -> tp.List[XMLElement]:
            result = list(net.getElementsByTagName(tag))
            if shuffle:
                random.shuffle(result)
            return result

        result = PTnet()
        doc: XMLDocument = xml_parse(pnml)
        net: XMLElement = doc.getElementsByTagName("net")[0]
        for num, xml in enumerate(get_nodes("place")):
            pid = get_attr(xml, "id")
            name = get_node_attr(xml, "name")
            init = int(get_node_attr(xml, "initialMarking", default="0"))
            result.add_place(Place(num, pid, name, result, init))
        for num, xml in enumerate(get_nodes("transition")):
            tid = get_attr(xml, "id")
            name = get_node_attr(xml, "name")
            result.add_trans(Trans(num, tid, name, result))
        for xml in get_nodes("arc"):
            aid = get_attr(xml, "id")
            src = get_attr(xml, "source")
            dst = get_attr(xml, "target")
            val = int(get_node_attr(xml, "inscription", default="1"))
            result.add_arc(Arc(aid, src, dst, val))
        struct: tp.List[XMLElement] = net.getElementsByTagName("structure")
        if len(struct) > 0:
            for num, unit in enumerate(struct[0].getElementsByTagName("unit")):
                try:
                    xml_places = unit.getElementsByTagName("places")[0]
                    places = xml_places.childNodes[0].nodeValue.split()
                except:
                    places = []
                result.add_unit(Unit(num, result, places))
        return result

    def _compile_prog_measure_weight_based(
            self,
            fun_name: str,
            weight: dict[Place, int],
            cw: tp.Callable[[str], None]
    ) -> None:
        cw("""
prog_t
""" + fun_name + """
(marking_t m) {
  prog_t result = 0;
  list_iter_t it;
  list_for_each(m->marked, it) {
    const marked_place_t * p = (marked_place_t *) list_iter_item(it);
    switch(p->pid) {
""")
        for p, w in weight.items():
           cw(f"      case {p.num}: result += {w}; break;\n") 
        cw("""
    }
  }
  return result;
}""")

    def compile_prog_measure_random(
            self,
            cw: tp.Callable[[str], None]
    ) -> None:
        """Compile a progress measure function based on randomisation."""
        nums = list(range(len(self.P)))
        random.shuffle(nums)
        weight = {p: w for (p, w) in zip(self.P, nums)}
        self._compile_prog_measure_weight_based(
            "prog_measure_random", weight, cw
        )

    def compile_prog_measure_struct(
            self,
            cw: tp.Callable[[str], None]
    ) -> None:
        """Compile a progress measure function based on net structure."""
        w = 1
        weight: dict[Place, int] = dict()
        for p in self.P:
            if p.init > 0:
                weight[p] = w
                w += 1
        new: set[Place] = set(weight)
        loop = True
        while loop:
            loop = False
            next_new: set[Place] = set()
            for p in new:
                for _, t in p.get_post_set():
                    for _, q in t.get_post_set():
                        if q not in weight:
                            assert isinstance(q, Place)
                            next_new.add(q)
                            weight[q] = w
                            w += 1
                            loop = True
            new = next_new
        self._compile_prog_measure_weight_based(
            "prog_measure", weight, cw
        )

    def compile_prog_measure_unit(
            self,
            cw: tp.Callable[[str], None]
    ) -> None:
        """Compile a progress measure function based on units."""
        weight: dict[Place, int] = dict()
        for u in self.U:
            p0 = next((self.get_node(pname) for pname in u.places if self.get_node(pname).init > 0), None)
            if p0 is not None:
                print(p0.name)
