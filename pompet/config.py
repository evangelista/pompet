#!/usr/bin/env python3

"""Implementation of pompet's configuration."""

import json
import typing as tp

from . import paths


config: tp.Dict[str, str] = {
    'ar': 'ar',
    'cc': 'gcc',
    'ar_opts': 'rcs',
    'cc_opts': '-std=gnu17 -Wall -O3',
    'link_opts': f'-L{paths.lib_dir}'
}


def load() -> None:
    """Load the configuration from paths.config_file."""
    global config
    try:
        with open(paths.config_file, encoding='UTF-8') as fd:
            config = json.loads(fd.read())
    except (FileNotFoundError, json.decoder.JSONDecodeError):
        with open(paths.config_file, 'w', encoding='utf-8') as fd:
            fd.write(json.dumps(config, indent=3) + '\n')
