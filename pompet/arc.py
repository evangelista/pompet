#!/usr/bin/env python3

"""Definition of class Arc."""

import typing as tp


class Arc:
    """An Arc links a place and a transition node."""

    def __init__(self, aid: str, src: str, dst: str, val: int):
        self.aid: str = aid
        self.src: str = src
        self.dst: str = dst
        self.val: int = val

    def get_other_end(self, nid: str) -> str:
        """If nid is the id of one end of self then return the other end."""
        if self.src == nid:
            return self.dst
        assert self.dst == nid
        return self.src

    def to_pnml(self, pr: tp.Callable[[str], None]) -> None:
        """Output to pnml."""
        out = f'<arc id="{self.aid}" '
        out += f'source="{self.src}" target="{self.dst}"'
        if self.val == 1:
            out += '/>\n'
        else:
            out += f'><inscription><text>{self.val}</text>'
            out += '</inscription></arc>\n'
        pr(out)
