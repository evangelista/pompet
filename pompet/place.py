#!/usr/bin/env python3

"""Definition of class Place."""

import typing as tp

from .node import Node
if tp.TYPE_CHECKING:
    from .ptnet import PTnet


class Place(Node):

    def __init__(self, num: int, nid: str, name: str, net: 'PTnet', init: int):
        Node.__init__(self, num, nid, name, net)
        self.init = init

    def to_prod(self, pr: tp.Callable[[str], None]) -> None:
        pr(f'#place P{self.num}')
        if self.init > 0:
            pr(' mk(' + str(self.init) + '<..>)')
        pr('\n')

    def to_pnml(self, pr: tp.Callable[[str], None]) -> None:
        out = f'<place id="{self.nid}">'
        out += f'<name><text>{self.name}</text></name>'
        if self.init > 0:
            out += f'<initialMarking><text>{self.init}</text></initialMarking>'
        out += '</place>\n'
        pr(out)

    def is_post_agglomerable(self) -> bool:
        pre = self.get_pre_set()
        post = self.get_post_set()

        # we do not reduce if |pre| and |post| is > 1.  otherwise this
        # may lead to a transition explosion
        if len(pre) > 1 and len(post) > 1:
            return False

        # self is not marked
        if self.init != 0:
            return False

        # all arcs around self have a valuation of 1
        if not all(p[0] == 1 for x in (pre, post) for p in x):
            return False

        # pre inter post is empty
        if not all(all(q[1] != p[1] for q in post) for p in pre):
            return False

        # self is the only input place of transitions of post
        if not all(len(t[1].get_pre_set()) == 1 for t in post):
            return False

        return True
