#!/usr/bin/env python3

"""Definition of class Node."""

import random
import string
import typing as tp

from .arc import Arc

if tp.TYPE_CHECKING:
    from .ptnet import PTnet


class Node:
    """A Node is either a Place either a Transition."""

    def __init__(
            self,
            num: int,
            nid: tp.Optional[str],
            name: str,
            net: 'PTnet'
    ):
        self.num: int = num
        if nid is not None:
            self.nid: str = nid
        else:
            self.nid = ''.join(
                random.choice(string.ascii_letters) for i in range(20)
            )
        self.name: str = name
        self.arcs: tp.List[Arc] = []
        self.net: 'PTnet' = net
        self.input_arcs: tp.List[Arc] = []
        self.output_arcs: tp.List[Arc] = []

    def to_prod(self, pr: tp.Callable[[str], None]) -> None:
        """Output to prod's input language."""

    def to_pnml(self, pr: tp.Callable[[str], None]) -> None:
        """Output to pnml."""

    def add_arc(self, arc: Arc) -> None:
        """Add arc to self's connected arcs."""
        assert self.nid in [arc.src, arc.dst]
        if arc.src == self.nid:
            self.output_arcs.append(arc)
        else:
            self.input_arcs.append(arc)
        self.arcs.append(arc)

    def _get_pre_or_post_set(
            self,
            arcs: tp.List[Arc]
    ) -> tp.List[tp.Tuple[int, 'Node']]:
        return sorted([
            (a.val, self.net.get_node(a.get_other_end(self.nid)))
            for a in arcs
        ], key=lambda val_node: int(val_node[1].num))

    def get_pre_set(self) -> tp.List[tp.Tuple[int, 'Node']]:
        """Return self's preset (°self in PN notation)."""
        return self._get_pre_or_post_set(self.input_arcs)

    def get_post_set(self) -> tp.List[tp.Tuple[int, 'Node']]:
        """Return self's postset (self° in PN notation)."""
        return self._get_pre_or_post_set(self.output_arcs)

    def get_updated_set(self) -> tp.List[tp.Tuple[int, 'Node']]:
        """Return the set of nodes linked to self with the valuations."""
        from .place import Place
        if isinstance(self, Place):
            dom = [ (self.input_arcs, 1), (self.output_arcs, -1)]
        else:
            dom = [ (self.input_arcs, -1), (self.output_arcs, 1)]
        l = [
            (m * a.val, self.net.get_node(a.get_other_end(self.nid)))
            for arcs, m in dom
            for a in arcs
        ]
        #  sort and merge the list
        l = sorted(l, key = lambda val_node: int(val_node[1].num))
        result = []
        prev = None
        for val, n in l:
            if prev is None:
                result.append((val, n))
                prev = val, n
            else:
                val_prev, n_prev = prev
                if n_prev == n:
                    result.pop()
                    if val_prev + val != 0:
                        result.append((val_prev + val, n))
                    prev = None
                else:
                    result.append((val, n))
                    prev = val, n
        return result
