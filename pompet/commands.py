#!/usr/bin/env python3

"""Implements commands provided by the pompet script."""

import os
import stat
import tempfile
import shutil
import subprocess
import typing as tp
import pkg_resources

from . import io, paths
from .config import config
from .ptnet import PTnet


def _exec(
        cmd: str,
        exit_on_error: bool = True
) -> None:
    io.msg(cmd)
    popen = subprocess.run(cmd, shell=True)
    if popen.returncode != 0:
        io.err(
            f'execution of "{cmd}" failed',
            popen.returncode if exit_on_error else None
        )


def _mkdir(
        the_dir: tp.Optional[str] = None
) -> str:
    if the_dir is None:
        result = tempfile.mkdtemp()
        io.msg(f'create {result}')
    else:
        result = the_dir
        io.msg(f'create {the_dir}')
        os.mkdir(the_dir)
    return result


def _cp_file(src: str, dst: str) -> None:
    io.msg(f'cp {src} {dst}')
    shutil.copyfile(src, dst)


def _cp_files(
        src: str,
        dst: str,
        exts: tp.Optional[tp.List[str]]
) -> tp.List[str]:
    result = list()
    for f in os.listdir(src):
        if exts is None or any(f.endswith(ext) for ext in exts):
            src_file = os.path.join(src, f)
            dst_file = os.path.join(dst, f)
            _cp_file(src_file, dst_file)
            result.append(f)
    return result


def _parse(
        pnml: str,
        shuffle: bool,
        reduce: bool
) -> PTnet:
    io.msg(f'parse {pnml}')
    result = PTnet.from_pnml(pnml, shuffle=shuffle)
    if reduce:
        io.msg('reduce net')
        result.reduce()
    return result


def to_prod(
        pnml: str,
        prod: str,
        shuffle: bool = False,
        reduce: bool = False
) -> None:
    """Convert pnml file to prod file."""
    net = _parse(pnml, shuffle, reduce)
    io.msg(f'output net to {prod}')
    net.to_prod(prod)


def to_pnml(
        pnml: str,
        pnml_out: str,
        shuffle: bool = False,
        reduce: bool = False
) -> None:
    """Convert pnml file to pnml file."""
    net = _parse(pnml, shuffle, reduce)
    io.msg(f'output net to {pnml_out}')
    net.to_pnml(pnml_out)


def compile_net(
        pnml: str,
        exe: str,
        shuffle: bool = False,
        reduce: bool = False,
        dep_rel_file: tp.Optional[str] = None,
        out_dir: tp.Optional[str] = None
) -> None:
    """Convert pnml file to an executable."""
    out_dir = _mkdir(out_dir)

    # check the checker library has already been generated
    if not os.path.isfile(paths.checker_lib_file):
        msg = 'checker\'s library not found.'
        msg += ' run "pompet compile-checker" first'
        io.err(msg)

    # copy all .h files of the checker in the output directory
    checker_dir = pkg_resources.resource_filename('pompet', 'checker')
    _cp_files(checker_dir, out_dir, exts=['.h'])

    # parse the pnml file and compile the net in C files
    net = _parse(pnml, shuffle, reduce)
    io.msg('compile net in c files')
    c_files = net.compile(out_dir, dep_rel_file=dep_rel_file)

    # go into the ouput directory and compile generated C files
    odir = os.getcwd()
    io.msg(f'cd {out_dir}')
    os.chdir(out_dir)
    o_files = list()
    for f in c_files:
        _exec(
            config['cc']
            + ' ' + config['cc_opts']
            + ' -c'
            + ' ' + f
        )
        o_files.append(f[:-2] + '.o')
    _exec(
        config['cc']
        + ' ' + config['cc_opts']
        + ' ' + ' '.join(o_files)
        + ' ' + config['link_opts']
        + ' -lcheckerpompet -o checker'
    )
    io.msg(f'cd {odir}')
    os.chdir(odir)

    # copy the executable
    src_file = os.path.join(out_dir, 'checker')
    _cp_file(src_file, exe)
    os.chmod(exe, os.stat(exe).st_mode | stat.S_IEXEC)


def compile_checker() -> None:
    """Compile the model checking library."""

    # copy all .h and .c files of the checker in the checker directory
    checker_dir = pkg_resources.resource_filename('pompet', 'checker')
    out_dir = paths.checker_dir
    files = _cp_files(checker_dir, out_dir, exts=['.h', '.c'])

    # go into the checker directory and compile everything
    odir = os.getcwd()
    io.msg(f'cd {out_dir}')
    os.chdir(out_dir)
    o_files = list()
    for f in files:
        if f.endswith('.c'):
            _exec(
                config['cc']
                + ' ' + config['cc_opts']
                + ' -c'
                + ' ' + f
            )
            o_files.append(f[:-2] + '.o')
    _exec(
        config['ar']
        + ' ' + config['ar_opts']
        + ' ' + paths.checker_lib_file
        + ' ' + ' '.join(o_files)
    )
    io.msg(f'cd {odir}')
    os.chdir(odir)
