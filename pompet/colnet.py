#!/usr/bin/env python3

"""Package containing functions to create and unfold colored Petri nets."""


import itertools
import typing as tp

from . import ptnet, place, trans, arc

value_t = tuple[tp.Any, ...] 

pnum = 0
tnum = 0
net = ptnet.PTnet()
all_places: dict[tuple[str, value_t], place.Place] = dict()
all_trans: dict[tuple[str, value_t], trans.Trans] = dict()

def _next_pnum() -> int:
    global pnum
    pnum = pnum + 1
    return pnum - 1


def _next_tnum() -> int:
    global tnum
    tnum = tnum + 1
    return tnum - 1


def empty_net() -> None:
    """Empty the current net."""
    global net
    global pnum
    global tnum
    global all_places
    global all_trans
    net = ptnet.PTnet()
    pnum = 0
    tnum = 0
    all_places = dict()
    all_trans = dict()


def new_place(
        name: str,
        dom: value_t,
        init: tp.Callable[[tp.Any], int],
        guard: tp.Callable[[value_t], bool]=lambda val: True
) -> None:
    """Create a new place."""
    for val in itertools.product(*dom):
        if not guard(val):
            continue
        pname = name + '_' + '_'.join(str(x) for x in val)
        p = place.Place(
            num=_next_pnum(),
            nid=pname,
            name=pname,
            net=net,
            init=init(val)
        )
        key = (name, tuple(val))
        all_places[key] = p
        net.add_place(p)


def new_trans(
        name: str,
        dom: value_t,
        in_arcs: dict[str, tp.Callable[[value_t], tuple[int, value_t]]],
        out_arcs: dict[str, tp.Callable[[value_t], tuple[int, value_t]]],
        guard: tp.Callable[[value_t], bool]=lambda val: True
) -> None:
    """Create a new transition."""
    for val in itertools.product(*dom):
        if not guard(val):
            continue
        tname = name + '_' + '_'.join(str(x) for x in val)
        t = trans.Trans(
            num=_next_tnum(),
            nid=tname,
            name=tname,
            net=net
        )
        key = (name, tuple(val))
        all_trans[key] = t
        net.add_trans(t)
        for pname, fun in in_arcs.items():
            aval, p_val = fun(val)
            p = all_places[pname, p_val]
            a = arc.Arc(
                aid=t.nid + '-' + p.nid,
                src=p.nid,
                dst=t.nid,
                val=aval
            )
            net.add_arc(a)
        for pname, fun in out_arcs.items():
            aval, p_val = fun(val)
            p = all_places[pname, p_val]
            a = arc.Arc(
                aid=p.nid + '-' + t.nid,
                src=t.nid,
                dst=p.nid,
                val=aval
            )
            net.add_arc(a)
