#!/usr/bin/env python3

"""Defines some meta data for pompet."""

NAME = 'pompet'

DESCRIPTION = 'Partial Order Model checker for PETri nets'

VERSION = '1.0.0'

URL = 'https://depot.lipn.univ-paris13.fr/evangelista/pompet'
